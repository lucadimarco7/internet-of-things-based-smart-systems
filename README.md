# Internet of Things Based Smart Systems
###### _Autori: Luca Dimarco - Antonino Salemi_
###### _Anno di realizzazione: 2023_

## Idea Progettuale
Nel repository è riportato il file `Presentazione.pdf` che espone l'idea progettuale.

## Note di Avvio dell'Applicazione
### Lato Client
Si è utilizzato Visual Studio Community, servono le librerie:
- Nethereum.Web3 (4.14.0>)
- Newtonsoft.Json (13.0.2>)
- System.Text.Json (7.0.1>)

C'è un file che potrebbe creare problemi perché si chiama UCOnlineWait.resx, si trova dentro "progettoIOT\Client\Client\Prova\Prova\UserControls\Game". Se dà errori andare sulle proprietà del file e spuntare "Annulla Blocco".

### Lato Server
Serve un database MongoDB, perché le domande vengono caricate li allo start-up. Inoltre anche lo username dei giocatori viene salvato lì.

Si devono inserire la chiave pubblica e la chiave privata dell'admin tra le "":
- chiave privata in tutte le variabili:
	- `privateKeyHex := ""` in `tokenFunction.go`
- chiave pubblica in tutte le variabile:
	- `superadmin_wallet string = ""` in `login-register.go`
	- `auth.From = common.HexToAddress("")` in `tokenFunction.go`

#### Note Aggiuntive
Username, punteggi locali, classifiche e contenuti vari che si trovano all'interno del progetto (commentati o inutilizzati) sono pensati per implementazioni future. Tutte le transazioni (che non coinvolgono l'admin) necessitano dell'estensione Metamask installata e attiva sul browser per essere firmate.
