// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.19;

contract QuizToken {
    string public name;
    string public symbol;
    uint8 public decimals;
    uint256 public totalSupply;
    uint256 public tokenPrice;
    address payable public admin;

    event TokensBought(address indexed buyer, uint256 amount, uint256 totalPrice);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Deposit(address indexed depositor, uint256 amount);

    mapping(address => uint256) public balanceOf;

    constructor(string memory _name, string memory _symbol, uint8 _decimals) {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;
        totalSupply = 1000000000;
        balanceOf[address(this)] = totalSupply; // i token vengono detenuti dal contratto
        admin = payable(msg.sender);
        tokenPrice = 1; // 1 wei
    }
    
    receive() external payable {
        // Controlla che la quantità di ETH inviata non sia zero
        require(msg.value > 0, "No ether sent");

        // Aggiunge la quantità di ETH inviata al bilancio del contratto
        balanceOf[address(this)] += msg.value;

        // Emette un evento per segnalare il deposito di ETH
        emit Deposit(msg.sender, msg.value);
    }


    function getContractTokenBalance() public view returns (uint256) {
        return balanceOf[address(this)];
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        address _from = msg.sender;
        require(balanceOf[_from] >= _value, "Not enough balance");
        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(_from, _to, _value);
        return true;
    }

    function withdrawTokens(uint256 _amount) public {
        require(msg.sender == admin, "Only admin can withdraw tokens");
        require(_amount <= balanceOf[address(this)], "Not enough tokens to withdraw");
        balanceOf[address(this)] -= _amount;
        balanceOf[msg.sender] += _amount;
    }

    function withdrawEther(uint256 _amount) public {
        require(msg.sender == admin, "Only admin can withdraw ether");
        require(_amount <= address(this).balance, "Not enough ether in the contract");
        admin.transfer(_amount);
    }

    function buyTokens(uint256 _amount) public payable {
        address _buyer = msg.sender;
        require(msg.value > 0, "No ether sent");
        uint256 totalPrice = _amount * tokenPrice;
        require(msg.value >= totalPrice, "Not enough ether sent");
        require(_amount <= balanceOf[address(this)], "Not enough tokens for sale");
        balanceOf[address(this)] -= _amount;
        balanceOf[_buyer] += _amount;
        emit TokensBought(_buyer, _amount, totalPrice);
    }

    function sellTokens(uint256 _amount) public {
        require(_amount >= 10, "Minimum amount to sell is 10");
        require(_amount <= balanceOf[msg.sender], "Not enough tokens to sell");
        uint256 totalPrice = _amount * 8 / 10;
        require(address(this).balance >= totalPrice, "Contract doesn't have enough ETH to buy tokens");
        balanceOf[address(this)] += _amount;
        balanceOf[msg.sender] -= _amount;
        payable(msg.sender).transfer(totalPrice);
    }
}