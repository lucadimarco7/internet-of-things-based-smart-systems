// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package tokenServices

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// QuizTokenMetaData contains all meta data concerning the QuizToken contract.
var QuizTokenMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"_symbol\",\"type\":\"string\"},{\"internalType\":\"uint8\",\"name\":\"_decimals\",\"type\":\"uint8\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"depositor\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"totalPrice\",\"type\":\"uint256\"}],\"name\":\"TokensBought\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"admin\",\"outputs\":[{\"internalType\":\"addresspayable\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"buyTokens\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getContractTokenBalance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"sellTokens\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"tokenPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"success\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdrawEther\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdrawTokens\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
	Bin: "0x60806040523480156200001157600080fd5b5060405162001ed838038062001ed88339818101604052810190620000379190620002ea565b8260009081620000489190620005cf565b5081600190816200005a9190620005cf565b5080600260006101000a81548160ff021916908360ff160217905550633b9aca00600381905550600354600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000208190555033600560006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506001600481905550505050620006b6565b6000604051905090565b600080fd5b600080fd5b600080fd5b600080fd5b6000601f19601f8301169050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b620001828262000137565b810181811067ffffffffffffffff82111715620001a457620001a362000148565b5b80604052505050565b6000620001b962000119565b9050620001c7828262000177565b919050565b600067ffffffffffffffff821115620001ea57620001e962000148565b5b620001f58262000137565b9050602081019050919050565b60005b838110156200022257808201518184015260208101905062000205565b60008484015250505050565b6000620002456200023f84620001cc565b620001ad565b90508281526020810184848401111562000264576200026362000132565b5b6200027184828562000202565b509392505050565b600082601f8301126200029157620002906200012d565b5b8151620002a38482602086016200022e565b91505092915050565b600060ff82169050919050565b620002c481620002ac565b8114620002d057600080fd5b50565b600081519050620002e481620002b9565b92915050565b60008060006060848603121562000306576200030562000123565b5b600084015167ffffffffffffffff81111562000327576200032662000128565b5b620003358682870162000279565b935050602084015167ffffffffffffffff81111562000359576200035862000128565b5b620003678682870162000279565b92505060406200037a86828701620002d3565b9150509250925092565b600081519050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b60006002820490506001821680620003d757607f821691505b602082108103620003ed57620003ec6200038f565b5b50919050565b60008190508160005260206000209050919050565b60006020601f8301049050919050565b600082821b905092915050565b600060088302620004577fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8262000418565b62000463868362000418565b95508019841693508086168417925050509392505050565b6000819050919050565b6000819050919050565b6000620004b0620004aa620004a4846200047b565b62000485565b6200047b565b9050919050565b6000819050919050565b620004cc836200048f565b620004e4620004db82620004b7565b84845462000425565b825550505050565b600090565b620004fb620004ec565b62000508818484620004c1565b505050565b5b81811015620005305762000524600082620004f1565b6001810190506200050e565b5050565b601f8211156200057f576200054981620003f3565b620005548462000408565b8101602085101562000564578190505b6200057c620005738562000408565b8301826200050d565b50505b505050565b600082821c905092915050565b6000620005a46000198460080262000584565b1980831691505092915050565b6000620005bf838362000591565b9150826002028217905092915050565b620005da8262000384565b67ffffffffffffffff811115620005f657620005f562000148565b5b620006028254620003be565b6200060f82828562000534565b600060209050601f83116001811462000647576000841562000632578287015190505b6200063e8582620005b1565b865550620006ae565b601f1984166200065786620003f3565b60005b8281101562000681578489015182556001820191506020850194506020810190506200065a565b86831015620006a157848901516200069d601f89168262000591565b8355505b6001600288020188555050505b505050505050565b61181280620006c66000396000f3fe6080604052600436106100c65760003560e01c80633bed33ce1161007f5780637ff9b596116100595780637ff9b5961461033957806395d89b4114610364578063a9059cbb1461038f578063f851a440146103cc576101b4565b80633bed33ce146102aa5780636c11bcd3146102d357806370a08231146102fc576101b4565b806306fdde03146101b957806318160ddd146101e4578063313ce5671461020f578063315a095d1461023a578063317d9453146102635780633610724e1461028e576101b4565b366101b4576000341161010e576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161010590610ef5565b60405180910390fd5b34600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600082825461015d9190610f4e565b925050819055503373ffffffffffffffffffffffffffffffffffffffff167fe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c346040516101aa9190610f91565b60405180910390a2005b600080fd5b3480156101c557600080fd5b506101ce6103f7565b6040516101db919061102b565b60405180910390f35b3480156101f057600080fd5b506101f9610485565b6040516102069190610f91565b60405180910390f35b34801561021b57600080fd5b5061022461048b565b6040516102319190611069565b60405180910390f35b34801561024657600080fd5b50610261600480360381019061025c91906110b5565b61049e565b005b34801561026f57600080fd5b5061027861065f565b6040516102859190610f91565b60405180910390f35b6102a860048036038101906102a391906110b5565b6106a6565b005b3480156102b657600080fd5b506102d160048036038101906102cc91906110b5565b6108c6565b005b3480156102df57600080fd5b506102fa60048036038101906102f591906110b5565b610a05565b005b34801561030857600080fd5b50610323600480360381019061031e9190611140565b610c22565b6040516103309190610f91565b60405180910390f35b34801561034557600080fd5b5061034e610c3a565b60405161035b9190610f91565b60405180910390f35b34801561037057600080fd5b50610379610c40565b604051610386919061102b565b60405180910390f35b34801561039b57600080fd5b506103b660048036038101906103b1919061116d565b610cce565b6040516103c391906111c8565b60405180910390f35b3480156103d857600080fd5b506103e1610e72565b6040516103ee9190611204565b60405180910390f35b600080546104049061124e565b80601f01602080910402602001604051908101604052809291908181526020018280546104309061124e565b801561047d5780601f106104525761010080835404028352916020019161047d565b820191906000526020600020905b81548152906001019060200180831161046057829003601f168201915b505050505081565b60035481565b600260009054906101000a900460ff1681565b600560009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461052e576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610525906112cb565b60405180910390fd5b600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020548111156105b0576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016105a790611337565b60405180910390fd5b80600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282546105ff9190611357565b9250508190555080600660003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282546106559190610f4e565b9250508190555050565b6000600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054905090565b6000339050600034116106ee576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016106e590610ef5565b60405180910390fd5b6000600454836106fe919061138b565b905080341015610743576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161073a90611419565b60405180910390fd5b600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020548311156107c5576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107bc90611485565b60405180910390fd5b82600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282546108149190611357565b9250508190555082600660008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600082825461086a9190610f4e565b925050819055508173ffffffffffffffffffffffffffffffffffffffff167f8442948036198f1146d3a63c3db355d7e0295c2cc5676c755990445da4fdc1c984836040516108b99291906114a5565b60405180910390a2505050565b600560009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614610956576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161094d9061151a565b60405180910390fd5b47811115610999576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161099090611586565b60405180910390fd5b600560009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051600060405180830381858888f19350505050158015610a01573d6000803e3d6000fd5b5050565b600a811015610a49576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610a40906115f2565b60405180910390fd5b600660003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054811115610acb576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610ac29061165e565b60405180910390fd5b6000600a600883610adc919061138b565b610ae691906116ad565b905080471015610b2b576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610b2290611750565b60405180910390fd5b81600660003073ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254610b7a9190610f4e565b9250508190555081600660003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254610bd09190611357565b925050819055503373ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051600060405180830381858888f19350505050158015610c1d573d6000803e3d6000fd5b505050565b60066020528060005260406000206000915090505481565b60045481565b60018054610c4d9061124e565b80601f0160208091040260200160405190810160405280929190818152602001828054610c799061124e565b8015610cc65780601f10610c9b57610100808354040283529160200191610cc6565b820191906000526020600020905b815481529060010190602001808311610ca957829003601f168201915b505050505081565b60008033905082600660008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020541015610d56576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610d4d906117bc565b60405180910390fd5b82600660008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254610da59190611357565b9250508190555082600660008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254610dfb9190610f4e565b925050819055508373ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef85604051610e5f9190610f91565b60405180910390a3600191505092915050565b600560009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600082825260208201905092915050565b7f4e6f2065746865722073656e7400000000000000000000000000000000000000600082015250565b6000610edf600d83610e98565b9150610eea82610ea9565b602082019050919050565b60006020820190508181036000830152610f0e81610ed2565b9050919050565b6000819050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b6000610f5982610f15565b9150610f6483610f15565b9250828201905080821115610f7c57610f7b610f1f565b5b92915050565b610f8b81610f15565b82525050565b6000602082019050610fa66000830184610f82565b92915050565b600081519050919050565b60005b83811015610fd5578082015181840152602081019050610fba565b60008484015250505050565b6000601f19601f8301169050919050565b6000610ffd82610fac565b6110078185610e98565b9350611017818560208601610fb7565b61102081610fe1565b840191505092915050565b600060208201905081810360008301526110458184610ff2565b905092915050565b600060ff82169050919050565b6110638161104d565b82525050565b600060208201905061107e600083018461105a565b92915050565b600080fd5b61109281610f15565b811461109d57600080fd5b50565b6000813590506110af81611089565b92915050565b6000602082840312156110cb576110ca611084565b5b60006110d9848285016110a0565b91505092915050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b600061110d826110e2565b9050919050565b61111d81611102565b811461112857600080fd5b50565b60008135905061113a81611114565b92915050565b60006020828403121561115657611155611084565b5b60006111648482850161112b565b91505092915050565b6000806040838503121561118457611183611084565b5b60006111928582860161112b565b92505060206111a3858286016110a0565b9150509250929050565b60008115159050919050565b6111c2816111ad565b82525050565b60006020820190506111dd60008301846111b9565b92915050565b60006111ee826110e2565b9050919050565b6111fe816111e3565b82525050565b600060208201905061121960008301846111f5565b92915050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b6000600282049050600182168061126657607f821691505b6020821081036112795761127861121f565b5b50919050565b7f4f6e6c792061646d696e2063616e20776974686472617720746f6b656e730000600082015250565b60006112b5601e83610e98565b91506112c08261127f565b602082019050919050565b600060208201905081810360008301526112e4816112a8565b9050919050565b7f4e6f7420656e6f75676820746f6b656e7320746f207769746864726177000000600082015250565b6000611321601d83610e98565b915061132c826112eb565b602082019050919050565b6000602082019050818103600083015261135081611314565b9050919050565b600061136282610f15565b915061136d83610f15565b925082820390508181111561138557611384610f1f565b5b92915050565b600061139682610f15565b91506113a183610f15565b92508282026113af81610f15565b915082820484148315176113c6576113c5610f1f565b5b5092915050565b7f4e6f7420656e6f7567682065746865722073656e740000000000000000000000600082015250565b6000611403601583610e98565b915061140e826113cd565b602082019050919050565b60006020820190508181036000830152611432816113f6565b9050919050565b7f4e6f7420656e6f75676820746f6b656e7320666f722073616c65000000000000600082015250565b600061146f601a83610e98565b915061147a82611439565b602082019050919050565b6000602082019050818103600083015261149e81611462565b9050919050565b60006040820190506114ba6000830185610f82565b6114c76020830184610f82565b9392505050565b7f4f6e6c792061646d696e2063616e207769746864726177206574686572000000600082015250565b6000611504601d83610e98565b915061150f826114ce565b602082019050919050565b60006020820190508181036000830152611533816114f7565b9050919050565b7f4e6f7420656e6f75676820657468657220696e2074686520636f6e7472616374600082015250565b6000611570602083610e98565b915061157b8261153a565b602082019050919050565b6000602082019050818103600083015261159f81611563565b9050919050565b7f4d696e696d756d20616d6f756e7420746f2073656c6c20697320313000000000600082015250565b60006115dc601c83610e98565b91506115e7826115a6565b602082019050919050565b6000602082019050818103600083015261160b816115cf565b9050919050565b7f4e6f7420656e6f75676820746f6b656e7320746f2073656c6c00000000000000600082015250565b6000611648601983610e98565b915061165382611612565b602082019050919050565b600060208201905081810360008301526116778161163b565b9050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601260045260246000fd5b60006116b882610f15565b91506116c383610f15565b9250826116d3576116d261167e565b5b828204905092915050565b7f436f6e747261637420646f65736e2774206861766520656e6f7567682045544860008201527f20746f2062757920746f6b656e73000000000000000000000000000000000000602082015250565b600061173a602e83610e98565b9150611745826116de565b604082019050919050565b600060208201905081810360008301526117698161172d565b9050919050565b7f4e6f7420656e6f7567682062616c616e63650000000000000000000000000000600082015250565b60006117a6601283610e98565b91506117b182611770565b602082019050919050565b600060208201905081810360008301526117d581611799565b905091905056fea2646970667358221220df37463c2179fca6e9e140ee94a731cb5b54b44c88cbc6ccf9b83093d48d203764736f6c63430008130033",
}

// QuizTokenABI is the input ABI used to generate the binding from.
// Deprecated: Use QuizTokenMetaData.ABI instead.
var QuizTokenABI = QuizTokenMetaData.ABI

// QuizTokenBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use QuizTokenMetaData.Bin instead.
var QuizTokenBin = QuizTokenMetaData.Bin

// DeployQuizToken deploys a new Ethereum contract, binding an instance of QuizToken to it.
func DeployQuizToken(auth *bind.TransactOpts, backend bind.ContractBackend, _name string, _symbol string, _decimals uint8) (common.Address, *types.Transaction, *QuizToken, error) {
	parsed, err := QuizTokenMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(QuizTokenBin), backend, _name, _symbol, _decimals)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &QuizToken{QuizTokenCaller: QuizTokenCaller{contract: contract}, QuizTokenTransactor: QuizTokenTransactor{contract: contract}, QuizTokenFilterer: QuizTokenFilterer{contract: contract}}, nil
}

// QuizToken is an auto generated Go binding around an Ethereum contract.
type QuizToken struct {
	QuizTokenCaller     // Read-only binding to the contract
	QuizTokenTransactor // Write-only binding to the contract
	QuizTokenFilterer   // Log filterer for contract events
}

// QuizTokenCaller is an auto generated read-only Go binding around an Ethereum contract.
type QuizTokenCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// QuizTokenTransactor is an auto generated write-only Go binding around an Ethereum contract.
type QuizTokenTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// QuizTokenFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type QuizTokenFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// QuizTokenSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type QuizTokenSession struct {
	Contract     *QuizToken        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// QuizTokenCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type QuizTokenCallerSession struct {
	Contract *QuizTokenCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// QuizTokenTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type QuizTokenTransactorSession struct {
	Contract     *QuizTokenTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// QuizTokenRaw is an auto generated low-level Go binding around an Ethereum contract.
type QuizTokenRaw struct {
	Contract *QuizToken // Generic contract binding to access the raw methods on
}

// QuizTokenCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type QuizTokenCallerRaw struct {
	Contract *QuizTokenCaller // Generic read-only contract binding to access the raw methods on
}

// QuizTokenTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type QuizTokenTransactorRaw struct {
	Contract *QuizTokenTransactor // Generic write-only contract binding to access the raw methods on
}

// NewQuizToken creates a new instance of QuizToken, bound to a specific deployed contract.
func NewQuizToken(address common.Address, backend bind.ContractBackend) (*QuizToken, error) {
	contract, err := bindQuizToken(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &QuizToken{QuizTokenCaller: QuizTokenCaller{contract: contract}, QuizTokenTransactor: QuizTokenTransactor{contract: contract}, QuizTokenFilterer: QuizTokenFilterer{contract: contract}}, nil
}

// NewQuizTokenCaller creates a new read-only instance of QuizToken, bound to a specific deployed contract.
func NewQuizTokenCaller(address common.Address, caller bind.ContractCaller) (*QuizTokenCaller, error) {
	contract, err := bindQuizToken(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &QuizTokenCaller{contract: contract}, nil
}

// NewQuizTokenTransactor creates a new write-only instance of QuizToken, bound to a specific deployed contract.
func NewQuizTokenTransactor(address common.Address, transactor bind.ContractTransactor) (*QuizTokenTransactor, error) {
	contract, err := bindQuizToken(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &QuizTokenTransactor{contract: contract}, nil
}

// NewQuizTokenFilterer creates a new log filterer instance of QuizToken, bound to a specific deployed contract.
func NewQuizTokenFilterer(address common.Address, filterer bind.ContractFilterer) (*QuizTokenFilterer, error) {
	contract, err := bindQuizToken(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &QuizTokenFilterer{contract: contract}, nil
}

// bindQuizToken binds a generic wrapper to an already deployed contract.
func bindQuizToken(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := QuizTokenMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_QuizToken *QuizTokenRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _QuizToken.Contract.QuizTokenCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_QuizToken *QuizTokenRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _QuizToken.Contract.QuizTokenTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_QuizToken *QuizTokenRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _QuizToken.Contract.QuizTokenTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_QuizToken *QuizTokenCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _QuizToken.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_QuizToken *QuizTokenTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _QuizToken.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_QuizToken *QuizTokenTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _QuizToken.Contract.contract.Transact(opts, method, params...)
}

// Admin is a free data retrieval call binding the contract method 0xf851a440.
//
// Solidity: function admin() view returns(address)
func (_QuizToken *QuizTokenCaller) Admin(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "admin")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Admin is a free data retrieval call binding the contract method 0xf851a440.
//
// Solidity: function admin() view returns(address)
func (_QuizToken *QuizTokenSession) Admin() (common.Address, error) {
	return _QuizToken.Contract.Admin(&_QuizToken.CallOpts)
}

// Admin is a free data retrieval call binding the contract method 0xf851a440.
//
// Solidity: function admin() view returns(address)
func (_QuizToken *QuizTokenCallerSession) Admin() (common.Address, error) {
	return _QuizToken.Contract.Admin(&_QuizToken.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_QuizToken *QuizTokenCaller) BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "balanceOf", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_QuizToken *QuizTokenSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _QuizToken.Contract.BalanceOf(&_QuizToken.CallOpts, arg0)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_QuizToken *QuizTokenCallerSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _QuizToken.Contract.BalanceOf(&_QuizToken.CallOpts, arg0)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_QuizToken *QuizTokenCaller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_QuizToken *QuizTokenSession) Decimals() (uint8, error) {
	return _QuizToken.Contract.Decimals(&_QuizToken.CallOpts)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_QuizToken *QuizTokenCallerSession) Decimals() (uint8, error) {
	return _QuizToken.Contract.Decimals(&_QuizToken.CallOpts)
}

// GetContractTokenBalance is a free data retrieval call binding the contract method 0x317d9453.
//
// Solidity: function getContractTokenBalance() view returns(uint256)
func (_QuizToken *QuizTokenCaller) GetContractTokenBalance(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "getContractTokenBalance")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetContractTokenBalance is a free data retrieval call binding the contract method 0x317d9453.
//
// Solidity: function getContractTokenBalance() view returns(uint256)
func (_QuizToken *QuizTokenSession) GetContractTokenBalance() (*big.Int, error) {
	return _QuizToken.Contract.GetContractTokenBalance(&_QuizToken.CallOpts)
}

// GetContractTokenBalance is a free data retrieval call binding the contract method 0x317d9453.
//
// Solidity: function getContractTokenBalance() view returns(uint256)
func (_QuizToken *QuizTokenCallerSession) GetContractTokenBalance() (*big.Int, error) {
	return _QuizToken.Contract.GetContractTokenBalance(&_QuizToken.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_QuizToken *QuizTokenCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_QuizToken *QuizTokenSession) Name() (string, error) {
	return _QuizToken.Contract.Name(&_QuizToken.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_QuizToken *QuizTokenCallerSession) Name() (string, error) {
	return _QuizToken.Contract.Name(&_QuizToken.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_QuizToken *QuizTokenCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_QuizToken *QuizTokenSession) Symbol() (string, error) {
	return _QuizToken.Contract.Symbol(&_QuizToken.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_QuizToken *QuizTokenCallerSession) Symbol() (string, error) {
	return _QuizToken.Contract.Symbol(&_QuizToken.CallOpts)
}

// TokenPrice is a free data retrieval call binding the contract method 0x7ff9b596.
//
// Solidity: function tokenPrice() view returns(uint256)
func (_QuizToken *QuizTokenCaller) TokenPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "tokenPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenPrice is a free data retrieval call binding the contract method 0x7ff9b596.
//
// Solidity: function tokenPrice() view returns(uint256)
func (_QuizToken *QuizTokenSession) TokenPrice() (*big.Int, error) {
	return _QuizToken.Contract.TokenPrice(&_QuizToken.CallOpts)
}

// TokenPrice is a free data retrieval call binding the contract method 0x7ff9b596.
//
// Solidity: function tokenPrice() view returns(uint256)
func (_QuizToken *QuizTokenCallerSession) TokenPrice() (*big.Int, error) {
	return _QuizToken.Contract.TokenPrice(&_QuizToken.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_QuizToken *QuizTokenCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _QuizToken.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_QuizToken *QuizTokenSession) TotalSupply() (*big.Int, error) {
	return _QuizToken.Contract.TotalSupply(&_QuizToken.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_QuizToken *QuizTokenCallerSession) TotalSupply() (*big.Int, error) {
	return _QuizToken.Contract.TotalSupply(&_QuizToken.CallOpts)
}

// BuyTokens is a paid mutator transaction binding the contract method 0x3610724e.
//
// Solidity: function buyTokens(uint256 _amount) payable returns()
func (_QuizToken *QuizTokenTransactor) BuyTokens(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.contract.Transact(opts, "buyTokens", _amount)
}

// BuyTokens is a paid mutator transaction binding the contract method 0x3610724e.
//
// Solidity: function buyTokens(uint256 _amount) payable returns()
func (_QuizToken *QuizTokenSession) BuyTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.BuyTokens(&_QuizToken.TransactOpts, _amount)
}

// BuyTokens is a paid mutator transaction binding the contract method 0x3610724e.
//
// Solidity: function buyTokens(uint256 _amount) payable returns()
func (_QuizToken *QuizTokenTransactorSession) BuyTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.BuyTokens(&_QuizToken.TransactOpts, _amount)
}

// SellTokens is a paid mutator transaction binding the contract method 0x6c11bcd3.
//
// Solidity: function sellTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactor) SellTokens(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.contract.Transact(opts, "sellTokens", _amount)
}

// SellTokens is a paid mutator transaction binding the contract method 0x6c11bcd3.
//
// Solidity: function sellTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenSession) SellTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.SellTokens(&_QuizToken.TransactOpts, _amount)
}

// SellTokens is a paid mutator transaction binding the contract method 0x6c11bcd3.
//
// Solidity: function sellTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactorSession) SellTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.SellTokens(&_QuizToken.TransactOpts, _amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _value) returns(bool success)
func (_QuizToken *QuizTokenTransactor) Transfer(opts *bind.TransactOpts, _to common.Address, _value *big.Int) (*types.Transaction, error) {
	return _QuizToken.contract.Transact(opts, "transfer", _to, _value)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _value) returns(bool success)
func (_QuizToken *QuizTokenSession) Transfer(_to common.Address, _value *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.Transfer(&_QuizToken.TransactOpts, _to, _value)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address _to, uint256 _value) returns(bool success)
func (_QuizToken *QuizTokenTransactorSession) Transfer(_to common.Address, _value *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.Transfer(&_QuizToken.TransactOpts, _to, _value)
}

// WithdrawEther is a paid mutator transaction binding the contract method 0x3bed33ce.
//
// Solidity: function withdrawEther(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactor) WithdrawEther(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.contract.Transact(opts, "withdrawEther", _amount)
}

// WithdrawEther is a paid mutator transaction binding the contract method 0x3bed33ce.
//
// Solidity: function withdrawEther(uint256 _amount) returns()
func (_QuizToken *QuizTokenSession) WithdrawEther(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.WithdrawEther(&_QuizToken.TransactOpts, _amount)
}

// WithdrawEther is a paid mutator transaction binding the contract method 0x3bed33ce.
//
// Solidity: function withdrawEther(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactorSession) WithdrawEther(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.WithdrawEther(&_QuizToken.TransactOpts, _amount)
}

// WithdrawTokens is a paid mutator transaction binding the contract method 0x315a095d.
//
// Solidity: function withdrawTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactor) WithdrawTokens(opts *bind.TransactOpts, _amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.contract.Transact(opts, "withdrawTokens", _amount)
}

// WithdrawTokens is a paid mutator transaction binding the contract method 0x315a095d.
//
// Solidity: function withdrawTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenSession) WithdrawTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.WithdrawTokens(&_QuizToken.TransactOpts, _amount)
}

// WithdrawTokens is a paid mutator transaction binding the contract method 0x315a095d.
//
// Solidity: function withdrawTokens(uint256 _amount) returns()
func (_QuizToken *QuizTokenTransactorSession) WithdrawTokens(_amount *big.Int) (*types.Transaction, error) {
	return _QuizToken.Contract.WithdrawTokens(&_QuizToken.TransactOpts, _amount)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_QuizToken *QuizTokenTransactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _QuizToken.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_QuizToken *QuizTokenSession) Receive() (*types.Transaction, error) {
	return _QuizToken.Contract.Receive(&_QuizToken.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_QuizToken *QuizTokenTransactorSession) Receive() (*types.Transaction, error) {
	return _QuizToken.Contract.Receive(&_QuizToken.TransactOpts)
}

// QuizTokenDepositIterator is returned from FilterDeposit and is used to iterate over the raw logs and unpacked data for Deposit events raised by the QuizToken contract.
type QuizTokenDepositIterator struct {
	Event *QuizTokenDeposit // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *QuizTokenDepositIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(QuizTokenDeposit)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(QuizTokenDeposit)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *QuizTokenDepositIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *QuizTokenDepositIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// QuizTokenDeposit represents a Deposit event raised by the QuizToken contract.
type QuizTokenDeposit struct {
	Depositor common.Address
	Amount    *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterDeposit is a free log retrieval operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed depositor, uint256 amount)
func (_QuizToken *QuizTokenFilterer) FilterDeposit(opts *bind.FilterOpts, depositor []common.Address) (*QuizTokenDepositIterator, error) {

	var depositorRule []interface{}
	for _, depositorItem := range depositor {
		depositorRule = append(depositorRule, depositorItem)
	}

	logs, sub, err := _QuizToken.contract.FilterLogs(opts, "Deposit", depositorRule)
	if err != nil {
		return nil, err
	}
	return &QuizTokenDepositIterator{contract: _QuizToken.contract, event: "Deposit", logs: logs, sub: sub}, nil
}

// WatchDeposit is a free log subscription operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed depositor, uint256 amount)
func (_QuizToken *QuizTokenFilterer) WatchDeposit(opts *bind.WatchOpts, sink chan<- *QuizTokenDeposit, depositor []common.Address) (event.Subscription, error) {

	var depositorRule []interface{}
	for _, depositorItem := range depositor {
		depositorRule = append(depositorRule, depositorItem)
	}

	logs, sub, err := _QuizToken.contract.WatchLogs(opts, "Deposit", depositorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(QuizTokenDeposit)
				if err := _QuizToken.contract.UnpackLog(event, "Deposit", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDeposit is a log parse operation binding the contract event 0xe1fffcc4923d04b559f4d29a8bfc6cda04eb5b0d3c460751c2402c5c5cc9109c.
//
// Solidity: event Deposit(address indexed depositor, uint256 amount)
func (_QuizToken *QuizTokenFilterer) ParseDeposit(log types.Log) (*QuizTokenDeposit, error) {
	event := new(QuizTokenDeposit)
	if err := _QuizToken.contract.UnpackLog(event, "Deposit", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// QuizTokenTokensBoughtIterator is returned from FilterTokensBought and is used to iterate over the raw logs and unpacked data for TokensBought events raised by the QuizToken contract.
type QuizTokenTokensBoughtIterator struct {
	Event *QuizTokenTokensBought // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *QuizTokenTokensBoughtIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(QuizTokenTokensBought)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(QuizTokenTokensBought)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *QuizTokenTokensBoughtIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *QuizTokenTokensBoughtIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// QuizTokenTokensBought represents a TokensBought event raised by the QuizToken contract.
type QuizTokenTokensBought struct {
	Buyer      common.Address
	Amount     *big.Int
	TotalPrice *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterTokensBought is a free log retrieval operation binding the contract event 0x8442948036198f1146d3a63c3db355d7e0295c2cc5676c755990445da4fdc1c9.
//
// Solidity: event TokensBought(address indexed buyer, uint256 amount, uint256 totalPrice)
func (_QuizToken *QuizTokenFilterer) FilterTokensBought(opts *bind.FilterOpts, buyer []common.Address) (*QuizTokenTokensBoughtIterator, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _QuizToken.contract.FilterLogs(opts, "TokensBought", buyerRule)
	if err != nil {
		return nil, err
	}
	return &QuizTokenTokensBoughtIterator{contract: _QuizToken.contract, event: "TokensBought", logs: logs, sub: sub}, nil
}

// WatchTokensBought is a free log subscription operation binding the contract event 0x8442948036198f1146d3a63c3db355d7e0295c2cc5676c755990445da4fdc1c9.
//
// Solidity: event TokensBought(address indexed buyer, uint256 amount, uint256 totalPrice)
func (_QuizToken *QuizTokenFilterer) WatchTokensBought(opts *bind.WatchOpts, sink chan<- *QuizTokenTokensBought, buyer []common.Address) (event.Subscription, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _QuizToken.contract.WatchLogs(opts, "TokensBought", buyerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(QuizTokenTokensBought)
				if err := _QuizToken.contract.UnpackLog(event, "TokensBought", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokensBought is a log parse operation binding the contract event 0x8442948036198f1146d3a63c3db355d7e0295c2cc5676c755990445da4fdc1c9.
//
// Solidity: event TokensBought(address indexed buyer, uint256 amount, uint256 totalPrice)
func (_QuizToken *QuizTokenFilterer) ParseTokensBought(log types.Log) (*QuizTokenTokensBought, error) {
	event := new(QuizTokenTokensBought)
	if err := _QuizToken.contract.UnpackLog(event, "TokensBought", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// QuizTokenTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the QuizToken contract.
type QuizTokenTransferIterator struct {
	Event *QuizTokenTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *QuizTokenTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(QuizTokenTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(QuizTokenTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *QuizTokenTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *QuizTokenTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// QuizTokenTransfer represents a Transfer event raised by the QuizToken contract.
type QuizTokenTransfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_QuizToken *QuizTokenFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*QuizTokenTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _QuizToken.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &QuizTokenTransferIterator{contract: _QuizToken.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_QuizToken *QuizTokenFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *QuizTokenTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _QuizToken.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(QuizTokenTransfer)
				if err := _QuizToken.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_QuizToken *QuizTokenFilterer) ParseTransfer(log types.Log) (*QuizTokenTransfer, error) {
	event := new(QuizTokenTransfer)
	if err := _QuizToken.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
