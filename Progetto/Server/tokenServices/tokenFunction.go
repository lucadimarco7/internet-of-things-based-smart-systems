package tokenServices

import (
	"context"
	"fmt"
	"log"
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

func WithdrawTokens(amount int64) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza del contratto MyToken
	myTokenAddress := common.HexToAddress("0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727")
	myTokenInstance, err := NewQuizToken(myTokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	privateKeyHex := ""

	privateKey, err := crypto.HexToECDSA(privateKeyHex)
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza della struttura TransactOpts con l'indirizzo del tuo account come From
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(5))
	if err != nil {
		log.Fatal(err)
	}
	auth.From = common.HexToAddress("")
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	auth.GasPrice = gasPrice

	// chiama la funzione withdrawTokens con l'istanza TransactOpts
	tx, err := myTokenInstance.WithdrawTokens(auth, big.NewInt(amount))
	if err != nil {
		log.Fatal(err)
		return ("Errore nella transazione")
	}

	// stampa l'hash della transazione
	fmt.Println("Transaction hash:", tx.Hash().Hex())
	return ("Transazione riuscita! Hash: " + tx.Hash().Hex())
}

func WithdrawEther(amount int64) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza del contratto MyToken
	myTokenAddress := common.HexToAddress("0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727")
	myTokenInstance, err := NewQuizToken(myTokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	privateKeyHex := ""

	privateKey, err := crypto.HexToECDSA(privateKeyHex)
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza della struttura TransactOpts con l'indirizzo del tuo account come From
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(5))
	if err != nil {
		log.Fatal(err)
	}
	auth.From = common.HexToAddress("")
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	auth.GasPrice = gasPrice

	// chiama la funzione withdrawTokens con l'istanza TransactOpts
	tx, err := myTokenInstance.WithdrawEther(auth, big.NewInt(amount))
	if err != nil {
		log.Fatal(err)
		return ("Errore nella transazione")
	}

	// stampa l'hash della transazione
	fmt.Println("Transaction hash:", tx.Hash().Hex())
	return ("Transazione riuscita! Hash: " + tx.Hash().Hex())
}

func Transfer(addressReceiverHex string, amount int64) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza del contratto MyToken
	myTokenAddress := common.HexToAddress("0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727")
	myTokenInstance, err := NewQuizToken(myTokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	privateKeyHex := ""

	privateKey, err := crypto.HexToECDSA(privateKeyHex)
	if err != nil {
		log.Fatal(err)
	}

	// crea un'istanza della struttura TransactOpts con l'indirizzo del tuo account come From
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(5))
	if err != nil {
		log.Fatal(err)
	}
	auth.From = common.HexToAddress("")
	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	auth.GasPrice = gasPrice

	addressReceiver := common.HexToAddress(addressReceiverHex)
	// chiama la funzione withdrawTokens con l'istanza TransactOpts
	tx, err := myTokenInstance.Transfer(auth, addressReceiver, big.NewInt(amount))
	if err != nil {
		log.Fatal(err)
		return ("Errore nella transazione")
	}

	// stampa l'hash della transazione
	fmt.Println("Transaction hash:", tx.Hash().Hex())
	return (tx.Hash().String())
}

func GetEth(addressSenderHex string) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	addressSender := common.HexToAddress(addressSenderHex)
	balance, err := client.BalanceAt(context.Background(), addressSender, nil)
	if err != nil {
		log.Fatal(err)
	}

	return balance.String()
}

func GetEthAvailability(addressSenderHex string) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	addressSender := common.HexToAddress(addressSenderHex)
	balance, err := client.BalanceAt(context.Background(), addressSender, nil)
	if err != nil {
		log.Fatal(err)
		return "Errore"
	}

	fmt.Println(gasPrice)
	//aggiungo 1000000 wei per lasciare un margine nel caso fluttuasse il prezzo del gas
	if balance.Cmp(gasPrice.Add(gasPrice, big.NewInt(1000000))) < 0 {
		return "Eth non sufficienti per la transazione"
	} else {
		return "Eth sufficienti"
	}
}

func HasPending(addressSenderHex string) string {
	// Connessione alla rete Ethereum
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		panic(err)
	}
	defer client.Close()
	// Ottenimento del codice bytecode dello stato pendente dell'indirizzo
	pendingCode, err := client.PendingCodeAt(context.Background(), common.HexToAddress(addressSenderHex))
	if err != nil {
		panic(err)
	}

	// Se il codice bytecode dello stato pendente non è vuoto, la funzione restituisce true
	if len(pendingCode) > 0 {
		return "true"
	}

	return "false"
}

func GetContractTokenBalance() string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	// Creare un'istanza del contratto MyToken usando il suo indirizzo
	tokenAddress := common.HexToAddress("0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727")
	token, err := NewQuizToken(tokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	// Chiamare la funzione getContractTokenBalance
	balance, err := token.GetContractTokenBalance(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}

	// Stampa il saldo del contratto
	fmt.Println("Contract token balance:", balance)
	return balance.String()
}

func GetAddressTokenBalance(addressHex string) string {
	// Creare un'istanza di client Ethereum Web3
	client, err := ethclient.Dial("https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161")
	if err != nil {
		log.Fatal(err)
	}

	// Creare un'istanza del contratto MyToken usando il suo indirizzo
	tokenAddress := common.HexToAddress("0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727")
	token, err := NewQuizToken(tokenAddress, client)
	if err != nil {
		log.Fatal(err)
		return ("Errore nella ricerca del wallet")
	}

	address := common.HexToAddress(addressHex)
	// Chiamare la funzione getContractTokenBalance
	balance, err := token.BalanceOf(&bind.CallOpts{}, address)
	if err != nil {
		log.Fatal(err)
		return ("Errore nella ricerca del bilancio del wallet")
	}

	// Stampa il saldo del wallet
	fmt.Println("Wallet token balance:", balance)
	return balance.String()
}
