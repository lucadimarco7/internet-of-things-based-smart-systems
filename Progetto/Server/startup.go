package main

import (
	"Server/data"
	"Server/utils"
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	admin_mail = "admin@admin.com"
	admin_psw  = "Az8jyTErX5g48kbAp/LypWlLvzZXpWzBPVXXNQ7Rm2Q="
	admin_name = "admin"
)

func load(mongodb *mongo.Database) {
	filter := bson.M{}
	collections, err := mongodb.ListCollectionNames(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	loadCollection := func(name string, loadFunc func(*mongo.Database)) {
		if !contains(collections, name) {
			loadFunc(mongodb)
		} else {
			fmt.Println("La collection " + name + " esiste, non verrà caricata da startUp")
		}
	}

	loadCollection("utenti", loadAdmin)
	loadCollection("questions", loadQuestion)
	loadCollection("categories", loadCategories)
}

func contains(collections []string, name string) bool {
	for _, collection := range collections {
		if collection == name {
			return true
		}
	}
	return false
}

func loadAdmin(mongodb *mongo.Database) {
	admin := data.UtenteRegister{}
	admin.WalletAddress = admin_mail
	admin.Nome = admin_name
	admin.Password = admin_psw
	admin.Exp = 0
	//admin.Crediti = 0
	utils.InsertOne("utenti", mongodb, admin)
}

func loadQuestion(mongodb *mongo.Database) {
	file, err := os.Open("db/questions.csv")
	if err != nil {
		fmt.Println("Errore durante l'apertura del file:", err)
		return
	}
	defer file.Close()

	// Crea un reader CSV
	reader := csv.NewReader(file)

	// Legge tutte le righe del file
	rows, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Errore durante la lettura delle righe:", err)
		return
	}
	for i, row := range rows {
		if i == 0 {

			continue
		}
		quest := data.QuestionInsert{
			Categoria:        row[0],
			Risposta:         row[1],
			Risposta2:        row[2],
			Risposta3:        row[3],
			RispostaCorretta: row[4],
			Testo:            row[5],
		}
		utils.InsertOne("questions", mongodb, quest)
	}
}

func loadCategories(mongodb *mongo.Database) {
	file, err := os.Open("db/categories.csv")
	if err != nil {
		fmt.Println("Errore durante l'apertura del file:", err)
		return
	}
	defer file.Close()

	// Crea un reader CSV
	reader := csv.NewReader(file)

	// Legge tutte le righe del file
	rows, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Errore durante la lettura delle righe:", err)
		return
	}
	for i, row := range rows {
		if i == 0 {

			continue
		}
		priceStr := strings.TrimSpace(row[1])
		price, err := strconv.ParseUint(priceStr, 10, 32)
		if err != nil {
			fmt.Println("Errore durante la conversione della stringa in uint32:", err)
			return
		}
		cat := data.CategorySign{
			Name:  row[0],
			Price: (uint32(price)),
		}
		utils.InsertOne("categories", mongodb, cat)
	}
}
