package main

import (
	"Server/data"
	"Server/service"
	"Server/tokenServices"
	"Server/utils"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"regexp"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
)

type MessagePT int

var users [MAX]string

const (
	register MessagePT = iota
	login
	sendQuiz
	endgame
	sendCategories
	insertQuestion
	sendQuestions
	updateQuestions
	deleteQuestion
	updateProfile
	buyCategory
	updateCategories
	deleteCategories
	sendProfile
	deleteUser
	sendStatistics
	closeConn
	multiplayer
	closeMultiplayer
	endGameOnline
	stopGameOnline
	logout
	getContractBalance
	withdrawTokens
	withdrawEth
	getWalletBalance
	getEthAvailability
	hasPending
	getEth
	buyTokens
	MAX_SALE = 5
	MAX      = 1000
)

func handleSession(conn net.Conn, mongodb *mongo.Database) {
	inputChannel := make(chan string, 1000)
	var (
		MP     MessagePT
		Token  string
		N_SALA int = -1
	)
	for {
		response := utils.Receive(conn)
		message := string(response)
		log.Println("Message Received: ", message)
		pt, err := SplitProtocol(message)
		if err != nil {
			conn.Close()
			break
		}
		//conversione ad Int
		MP_conv, _ := (strconv.Atoi(pt[0]))
		MP = MessagePT(MP_conv)
		Mjson := pt[1]
		log.Println("Messaggio Protocollo: ", MP, "Messaggio: ", Mjson)
		switch MP {

		case register:
			log.Println("Register --")
			service.Register(Mjson, conn, mongodb)

		case login:
			log.Println("Login --")
			service.Login(inputChannel, users[:], Mjson, conn, mongodb)
			Token = <-inputChannel
			insertUser(Token)
			fmt.Println(users)

		case sendQuiz:
			log.Println("sendQuiz --")
			service.SendQuiz(Mjson, conn, mongodb)

		case endgame:
			log.Println("EndGame--")
			service.EndGame(Token, Mjson, conn, mongodb)

		case sendCategories:
			log.Println("sendCategories --")
			service.SendCategories(Mjson, Token, conn, mongodb)

		case insertQuestion:
			log.Println("InsertQuestion --")
			service.InsertQuestion(Mjson, conn, mongodb)

		case sendQuestions:
			log.Println("sendQuestions --")
			service.SendQuestions(Mjson, conn, mongodb)

		case updateQuestions:
			log.Println("updateQuestions --")
			service.UpdateQuestion(Mjson, conn, mongodb)

		case deleteQuestion:
			log.Println("deleteQuestion --")
			service.DeleteQuestion(Mjson, conn, mongodb)

		case updateProfile:
			log.Println("updateProfile --")
			res := service.UpdateProfile(inputChannel, Mjson, Token, conn, mongodb)
			if res {
				logoutUser(Token)
				Token = <-inputChannel
				insertUser(Token)
				fmt.Println(users)
			}

		case buyCategory:
			log.Println("buyCategory--")
			service.BuyCategory(Mjson, Token, conn, mongodb)

		case updateCategories:
			log.Println("updateCategories --")
			service.UpdateCategories(Mjson, conn, mongodb)

		case deleteCategories:
			log.Println("deleteCategories --")
			service.DeleteCategories(Mjson, conn, mongodb)

		case sendProfile:
			log.Println("sendProfile --")
			service.SendProfile(Mjson, Token, conn, mongodb)

		case deleteUser:
			log.Println("deleteUser --")
			service.DeleteUser(Mjson, conn, mongodb)

		case sendStatistics:
			log.Println("sendStatistics")
			res := utils.SendToModule(Mjson)
			utils.Send(res, conn)

		case closeConn:
			log.Println("close --")
			logoutUser(Token)
			fmt.Println(users)
			close(inputChannel)
			conn.Close()
			return

		case multiplayer:
			fmt.Println("multi")
			go multiplayerRoutine(Token, &N_SALA, conn, mongodb)

		case closeMultiplayer:
			if N_SALA != -1 {
				Sale_Attesa[N_SALA].Ready = false
				LiberaSala(&N_SALA, Token)
			}

		case endGameOnline:
			log.Println("EndGameOnline--")
			if N_SALA != -1 {
				go EndGameOnline(Mjson, N_SALA, &N_SALA, Token, conn, mongodb)
			} else {
				msg := "err"
				utils.Send([]byte(msg), conn)
			}
		case stopGameOnline:
			log.Println("stopGameOnline--")
			N_SALA = -1

		case logout:
			log.Println("logout--")
			logoutUser(Token)
			fmt.Println(users)

		case getContractBalance:
			log.Println("getContractBalance--")
			msg := tokenServices.GetContractTokenBalance() //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case withdrawTokens:
			log.Println("withdrawTokens--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			amount := data["amount"].(float64)
			fmt.Println(amount)
			msg := tokenServices.WithdrawTokens(int64(amount)) //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case withdrawEth:
			log.Println("withdrawEth--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			amount := data["amount"].(float64)
			fmt.Println(amount)
			msg := tokenServices.WithdrawEther(int64(amount)) //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case getWalletBalance:
			log.Println("getWalletBalance--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			wallet_address := data["wallet_address"].(string)
			msg := tokenServices.GetAddressTokenBalance(wallet_address) //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case getEthAvailability:
			log.Println("getEthAvailability--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			wallet_address := data["wallet_address"].(string)
			msg := tokenServices.GetEthAvailability(wallet_address) //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case hasPending:
			log.Println("isPending--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			wallet_address := data["wallet_address"].(string)
			msg := tokenServices.HasPending(wallet_address) //Restituisce il saldo del contratto
			utils.Send([]byte(msg), conn)

		case getEth:
			log.Println("getEth--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			wallet_address := data["wallet_address"].(string)
			msg := tokenServices.GetEth(wallet_address)
			fmt.Println("Eth: " + msg)
			utils.Send([]byte(msg), conn)

			/*case buyTokens:
			log.Println("buyTokens--")
			message := message[3:]
			var data map[string]interface{}
			err := json.Unmarshal([]byte(message), &data)
			if err != nil {
				fmt.Println("Errore nell'analisi della stringa JSON: ", err)
				return
			}
			wallet_address := data["wallet_address"].(string)
			amount := int64(data["tokens"].(float64))
			msg := tokenServices.BuyTokens(wallet_address, amount)
			utils.Send([]byte(msg), conn)*/
		}
	}
}

func EndGameOnline(u_json string, sala int, salaP *int, Token string, conn net.Conn, mongodb *mongo.Database) {
	score := data.EndGameDataRcv{}
	json.Unmarshal([]byte(u_json), &score)
	notmyplace, myplace := ritornaPosto(Token, sala)
	Sale_Attesa[sala].Scores[myplace] = score.Score
	if Sale_Attesa[sala].Scores[notmyplace] != -1 {
		notmyscore := Sale_Attesa[sala].Scores[notmyplace]
		service.EndGameOnline(notmyscore, score.Score, Token, score, conn, mongodb)
		*salaP = -1
		return
	} else {
		//se il game non e' finito attendo il risultato oppure per un determinato numero di secondi
		var i int = 0
		for Sale_Attesa[sala].Scores[notmyplace] == -1 {
			if *salaP == -1 {
				msg := "EXIT"
				fmt.Println(msg)
				utils.Send([]byte(msg), conn)
				return
			}
			time.Sleep(500 * time.Millisecond)
			i++
			msg := "wait"
			utils.Send([]byte(msg), conn)
			if i > 60 {
				break
			}
		}
		var notmyscore int
		if i > 60 {
			notmyscore = 0
		} else {
			notmyscore = Sale_Attesa[sala].Scores[notmyplace]
		}

		service.EndGameOnline(notmyscore, score.Score, Token, score, conn, mongodb)
		*salaP = -1
		time.Sleep(2000 * time.Millisecond)
		return
	}
}

func SplitProtocol(message string) ([]string, error) {
	/*
		Il protocollo ha il seguente formato
		| protocol id | space | message | newline |
	*/
	var err error
	reg := regexp.MustCompile(`(\d*)\s(.*)?`)
	// n = -1 indica al metodo di ritornare tutte le submatch
	res := reg.FindAllStringSubmatch(message, -1)
	pt := make([]string, 2)
	if len(res) > 0 {
		pt[0] = res[0][1]
		pt[1] = res[0][2]
	} else {
		err = net.ErrClosed
	}
	return pt, err
}

func insertUser(Token string) {
	for i := 0; i < MAX; i++ {
		if len(users[i]) == 0 {
			users[i] = Token
			return
		}
	}
}

func multiplayerRoutine(token string, N_SALA *int, conn net.Conn, mongodb *mongo.Database) {
	riempiSala(token, N_SALA)
	//mentre sei in una sala
	for *N_SALA != -1 {
		sala := *N_SALA
		fmt.Println(sala)
		fmt.Println(Sale_Attesa[sala].Ready)
		//se la sala è ready party
		if Sale_Attesa[sala].Ready {
			Sale_Attesa[sala].Questions = service.MultQuestion(conn, mongodb)
			json_comp, _ := json.Marshal(Sale_Attesa[sala].Questions)
			utils.Send(json_comp, conn)
			/*wallet_address_US1, err := utils.GetWalletAddressByToken(Sale_Attesa[sala].US1, mongodb)
			wallet_address_US2, err2 := utils.GetWalletAddressByToken(Sale_Attesa[sala].US2, mongodb)
			if err != nil || err2 != nil {
				log.Fatal(err)
				msg := "Errore nella ricerca del wallet"
				utils.Send([]byte(msg), conn)
				return
			} else {
				tokenServices.Transfer(wallet_address_US1, "0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727", 5)
				tokenServices.Transfer(wallet_address_US2, "0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727", 5)
			}*/
			Sale_Attesa[sala].resetSala(sala)
			return
		} else if !Sale_Attesa[sala].Ready {
			//se la sala non è pronta aspetta
			for !Sale_Attesa[sala].Ready {
				sala = *N_SALA
				//se la sala è -1 vuol dire che l'utente ha annullato il matchmaking e deve uscire
				if sala == -1 {
					msg := "EXIT"
					fmt.Println(msg)
					utils.Send([]byte(msg), conn)
					return
				}
				time.Sleep(500 * time.Millisecond)
				msg := "wait"
				fmt.Println(msg)
				utils.Send([]byte(msg), conn)
			}
			json_comp, _ := json.Marshal(Sale_Attesa[sala].Questions)
			fmt.Println("invio a" + token)
			utils.Send(json_comp, conn)
			Sale_Attesa[sala].resetSala(sala)
			return
		}
	}
	msg := "Server pieno"
	utils.Send([]byte(msg), conn)
}

func logoutUser(Token string) {
	for i := 0; i < MAX; i++ {
		if users[i] == Token {
			users[i] = ""
			return
		}
	}
}

func riempiSala(Token string, N_SALA *int) {
	for i := 0; i < MAX_SALE; i++ {
		if len(Sale_Attesa[i].US1) == 0 {
			Sale_Attesa[i].US1 = Token
			*N_SALA = i
			if len(Sale_Attesa[i].US2) != 0 {
				Sale_Attesa[i].Ready = true
				return
			} else {
				Sale_Attesa[i].Ready = false
				return
			}

		} else if len(Sale_Attesa[i].US2) == 0 {
			Sale_Attesa[i].US2 = Token
			*N_SALA = i
			if len(Sale_Attesa[i].US1) != 0 {
				Sale_Attesa[i].Ready = true
				return
			} else {
				Sale_Attesa[i].Ready = false
				return
			}
		}

	}
	*N_SALA = -1
}

func ritornaPosto(token string, sala int) (int, int) {
	if Sale_Attesa[sala].US1 == token {
		return 1, 0
	}
	return 0, 1
}

func LiberaSala(N_SALA *int, Token string) {
	if *N_SALA != -1 {
		if Sale_Attesa[*N_SALA].US1 == Token {
			Sale_Attesa[*N_SALA].US1 = ""
			Sale_Attesa[*N_SALA].Ready = false
			Sale_Attesa[*N_SALA].Scores[0] = -1
		} else {
			Sale_Attesa[*N_SALA].US2 = ""
			Sale_Attesa[*N_SALA].Ready = false
			Sale_Attesa[*N_SALA].Scores[0] = -1
		}
		*N_SALA = -1
	}
}
