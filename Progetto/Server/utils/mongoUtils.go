package utils

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var ctx = context.Background()

func FindOne(filter primitive.D, name_coll string, mongodb *mongo.Database) *mongo.SingleResult {
	coll := mongodb.Collection(name_coll)
	result := coll.FindOne(ctx, filter)
	return result
}

func InsertOne(name_coll string, mongodb *mongo.Database, newelement interface{}) error {
	coll := mongodb.Collection(name_coll)
	_, err := coll.InsertOne(ctx, newelement)
	return err
}

func FindAll(name_coll string, mongodb *mongo.Database, filter primitive.D, res interface{}) {

	coll := mongodb.Collection(name_coll)
	cursor, err := coll.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	if err = cursor.All(ctx, res); err != nil {
		log.Fatal(err)
	}

}

func UpdateOne(name_coll string, mongodb *mongo.Database, filter primitive.D, update primitive.D) error {
	coll := mongodb.Collection(name_coll)
	_, err := coll.UpdateOne(ctx, filter, update)
	return err
}

func UpdateMany(name_coll string, mongodb *mongo.Database, filter primitive.D, update primitive.D) error {
	coll := mongodb.Collection(name_coll)
	_, err := coll.UpdateMany(ctx, filter, update)
	return err
}

func DeleteOne(name_coll string, mongodb *mongo.Database, filter primitive.D) error {
	coll := mongodb.Collection(name_coll)
	_, err := coll.DeleteOne(ctx, filter)
	return err
}
func DeleteMany(name_coll string, mongodb *mongo.Database, filter primitive.D) error {
	coll := mongodb.Collection(name_coll)
	_, err := coll.DeleteMany(ctx, filter)
	return err
}

//-----

func Aggregate(name_coll string, mongodb *mongo.Database, pipe bson.A, res interface{}) {
	coll := mongodb.Collection(name_coll)
	cursor, err := coll.Aggregate(ctx, pipe)
	if err != nil {
		panic(err)
	}
	defer cursor.Close(ctx)
	if err = cursor.All(ctx, res); err != nil {
		panic(err)
	}
}

func GetWalletAddressByToken(token string, mongodb *mongo.Database) (string, error) {
	filter := primitive.M{"password": token}
	name_coll := "utenti"
	coll := mongodb.Collection(name_coll)
	result := coll.FindOne(ctx, filter)

	var user map[string]interface{}
	err := result.Decode(&user)
	if err != nil {
		return "", err
	}

	wallet_address := user["wallet_address"].(string)
	return wallet_address, nil
}
