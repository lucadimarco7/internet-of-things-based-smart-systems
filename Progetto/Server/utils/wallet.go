package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"github.com/nanmu42/etherscan-api"
)

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Result  string `json:"result"`
}

func CheckWalletExistence(walletAddress string) (bool, error) {
	etherscanAPIKey := "HSQVVWYE6QTPD7SQ1QZMQQP6SGQ31W6YG9"
	url := fmt.Sprintf("https://api.etherscan.io/api?module=account&action=balance&address=%s&tag=latest&apikey=%s", walletAddress, etherscanAPIKey)
	resp, err := http.Get(url)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return false, err
	}
	if response.Status == "1" {
		return true, nil
	}
	return false, nil
}

func GetBalance(walletAddress common.Address) (*big.Int, error) {
	// Crea un client con la tua ApiKeyToken
	client := etherscan.New(etherscan.Goerli, "HSQVVWYE6QTPD7SQ1QZMQQP6SGQ31W6YG9")

	tokenContract := common.HexToAddress("0x22874EB26E19eB0Bc93BA9C2EBb808FcDC8791d4")

	// Ottieni il saldo del token
	balance, err := client.TokenBalance(tokenContract.String(), walletAddress.String())
	if err != nil {
		return nil, err
	}

	return balance.Int(), nil
}
