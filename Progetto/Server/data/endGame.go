package data

import "math/big"

type EndGameDataRcv struct {
	Game          string `bson:"game" json:"game"`
	Category      string `bson:"category" json:"category"`
	Score         int    `bson:"score" json:"punteggio"`
	LongestSeries int    `bson:"longestSeries" json:"longestSeries"`
	UsedHelps     int    `bson:"usedHelps" json:"usedHelps"`
}

type EndGameDataSnd struct {
	ResUpdate       bool      `bson:"resUpdate" json:"resUpdate"`
	LevelUp         bool      `bson:"levelUp" json:"levelUp"`
	LevelUser       LevelUser `bson:"leveluser" json:"leveluser"`
	Crediti         big.Int   `bson:"crediti" json:"crediti"`
	ChallengeString string    `bson:"chString" json:"chString"`
}
