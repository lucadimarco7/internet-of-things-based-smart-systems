package data

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Question struct {
	Codice           primitive.ObjectID `bson:"_id" json:"codice"`
	Categoria        string             `bson:"categoria" json:"categoria"`
	Testo            string             `bson:"testo" json:"testo"`
	Risposta         string             `bson:"risposta" json:"risposta"`
	Risposta2        string             `bson:"risposta2" json:"risposta2"`
	Risposta3        string             `bson:"risposta3" json:"risposta3"`
	RispostaCorretta string             `bson:"rispostaCorretta" json:"rispostaCorretta"`
}

type QuestionInsert struct {
	Categoria        string `bson:"categoria" json:"categoria"`
	Testo            string `bson:"testo" json:"testo"`
	Risposta         string `bson:"risposta" json:"risposta"`
	Risposta2        string `bson:"risposta2" json:"risposta2"`
	Risposta3        string `bson:"risposta3" json:"risposta3"`
	RispostaCorretta string `bson:"rispostaCorretta" json:"rispostaCorretta"`
}

type QuestionsRequest struct {
	Categoria string `bson:"categoria" json:"categoria"`
	Game      string `json:"game" bson:"game"`
}

type Request struct {
	TypeRequest string `bson:"request" json:"request"`
}

type Category struct {
	CategoryCode primitive.ObjectID `bson:"_id" json:"_id"`
	Name         string             `json:"name" bson:"name"`
	Price        uint               `json:"price" bson:"price"`
}
type CategorySign struct {
	Name  string `json:"name" bson:"name"`
	Price uint32 `json:"price" bson:"price"`
}
type Record struct {
	UserCode     primitive.ObjectID `bson:"usercode" json:"usercode"`
	CategoryCode primitive.ObjectID `bson:"categorycode" json:"categorycode"`
	Score        int                `bson:"score" json:"score"`
}
type Stats struct {
	GameCode     string             `json:"gamecode" bson:"gamecode"`
	CategoryCode primitive.ObjectID `bson:"categorycode" json:"categorycode"`
	TotalGame    uint               `bson:"totalgame" json:"totalgame"`
	Totalscore   uint               `bson:"totalscore" json:"totalscore"`
}
