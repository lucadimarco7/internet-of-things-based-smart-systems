package data

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Update struct {
	Codice           primitive.ObjectID `bson:"_id" json:"codice"`
	CampoModificato  string             `bson:"CampoModificato" json:"CampoModificato"`
	NuovoValore      string             `bson:"NuovoValore" json:"NuovoValore"`
	PasswordCorrente string             `bson:"PasswordCorrente" json:"PasswordCorrente"`
}
