package data

import (
	"math/big"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Utente struct {
	Codice        primitive.ObjectID `bson:"_id" json:"codice"`
	WalletAddress string             `bson:"wallet_address" json:"wallet_address"`
	Nome          string             `bson:"nome" json:"nome"`
	Password      string             `bson:"password" json:"password"`
	Exp           int                `bson:"exp" json:"exp"`
	LevelUser     LevelUser          `bson:"leveluser" json:"leveluser"`
	Balance       *big.Int           `bson:"balance" json:"balance"`
}
type UtenteRegister struct {
	WalletAddress string `bson:"wallet_address" json:"wallet_address"`
	Nome          string `bson:"nome" json:"nome"`
	Password      string `bson:"password" json:"password"`
	Exp           uint   `bson:"exp" json:"exp"`
}

type UserCredentials struct {
	Email         string `json:"email"`
	Password      string `json:"password"`
	WalletAddress string `json:"wallet_address"`
}

type LevelUser struct {
	Level int `bson:"level" json:"level"`
	Exp   int `bson:"exp" json:"exp"`
}

type CategoryBuy struct {
	BuyerCode    primitive.ObjectID `bson:"buyer" json:"buyer"`
	CategoryCode primitive.ObjectID `bson:"category" json:"category"`
}
