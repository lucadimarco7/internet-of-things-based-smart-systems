let web3;
if (typeof window.ethereum !== 'undefined') {
  // Rileva la presenza di MetaMask
  web3 = new Web3(window.ethereum);
} else {
  // Utilizza un provider Ethereum di fallback
  web3 = new Web3('https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161');
}

window.onload = function() {
// Richiedi l'accesso all'account
window.ethereum.enable()
  .then(accounts => {
    const fromAddress = accounts[0];

    const contractABI = [{"inputs":[{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_symbol","type":"string"},{"internalType":"uint8","name":"_decimals","type":"uint8"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"depositor","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"buyer","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"totalPrice","type":"uint256"}],"name":"TokensBought","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[],"name":"admin","outputs":[{"internalType":"address payable","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"buyTokens","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getContractTokenBalance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"sellTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"tokenPrice","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"withdrawEther","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"withdrawTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"stateMutability":"payable","type":"receive"}]; // inserisci qui l'ABI del tuo contratto
    const contractAddress = '0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727'; // inserisci qui l'indirizzo del tuo contratto

    const contractInstance = new web3.eth.Contract(contractABI, contractAddress);


    // Funzione per leggere i valori dei parametri della query dall'URL
    function getQueryParam(name) {
        const urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(name);
      }
      
      // Leggi i valori dei parametri dalla query dell'URL
      const to = getQueryParam('to');
      const value = getQueryParam('value');
      
      // Chiamata alla funzione buyTokens con i valori letti dall'URL
      contractInstance.methods.transfer(to, value).send({from: fromAddress})
        .on('transactionHash', hash => {
          console.log(`Transaction hash: ${hash}`);
        })
        .on('receipt', receipt => {
          console.log(`Transaction was mined in block ${receipt.blockNumber}`);
          //selezioniamo l'elemento con id "myDiv"
          const div = document.getElementById("request");
          document.getElementById("loading-bar-spinner").style.display = "none";
          //cambiamo il contenuto dell'elemento utilizzando il metodo innerHTML
          div.innerHTML = "Transazione approvata, puoi chiudere la pagina!";
          fetch("http://localhost:8080/transfer.html", {
            method: "POST",
            body: JSON.stringify({ message: "OK" })
          });
        })
        .on('error', error => {
          console.error(error);
          //selezioniamo l'elemento con id "myDiv"
          const div = document.getElementById("request");
          document.getElementById("loading-bar-spinner").style.display = "none";
          //cambiamo il contenuto dell'elemento utilizzando il metodo innerHTML
          div.innerHTML = "Transazione annullata, puoi chiudere la pagina!";
                fetch("http://localhost:8080/transfer.html", {
            method: "POST",
            body: JSON.stringify({ message: "ERROR" })
          });
        });
  });
}
