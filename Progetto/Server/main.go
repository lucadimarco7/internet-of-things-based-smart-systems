package main

import (
	"Server/data"
	"context"
	"fmt"
	"log"
	"net"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type sala struct {
	US1       string
	US2       string
	Ready     bool
	Questions []data.Question
	Scores    [2]int
}

func (s *sala) resetSala(x int) {
	time.Sleep(120000 * time.Millisecond)
	s.Scores[0] = -1
	s.Scores[1] = -1
	s.Ready = false
	s.US1 = ""
	s.US2 = ""
	fmt.Println(" sala numero: " + strconv.FormatUint(uint64(x), 10) + "libera")
}

var Sale_Attesa [MAX_SALE]sala

// Database URL
const uri = "mongodb://127.0.0.1:27017"

func main() {

	//-----------------------------------------------

	log.Println("Avvio server...")
	ln, err := net.Listen("tcp", ":13000")
	if err != nil {
		log.Println("Errore ascolto porta:", err)
	}
	//--------------CONNESSIONE DATABASE-----------------------------
	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	mongodb := client.Database("apl_database")
	go load(mongodb)
	go initSale()

	//------------------------------------------------------------------
	for {
		conn, err := ln.Accept()
		fmt.Println("Nuova richiesa")
		if err != nil {
			log.Println("Errore richiesta:", err)
		}
		go handleSession(conn, mongodb)
	}

}

func initSale() {
	for i := 0; i < MAX_SALE; i++ {
		Sale_Attesa[i].Scores[0] = -1
		Sale_Attesa[i].Scores[1] = -1
	}
}
