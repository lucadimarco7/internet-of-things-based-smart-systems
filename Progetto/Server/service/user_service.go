package service

import (
	"Server/data"
	"Server/utils"
	"encoding/json"
	"fmt"
	"net"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func UpdateProfile(out chan string, u_json string, token string, conn net.Conn, mongodb *mongo.Database) bool {
	dataUpdate := data.Update{}
	var update bson.D
	var filter bson.D
	res := data.Utente{}
	var newToken string
	json.Unmarshal([]byte(u_json), &dataUpdate)
	//questa parte serve a verificare che l'utente sia presente nel db per evitare errori e prendere l'email se si cambia la password
	//se si è un admin la richiesta va fatta dopo
	if token != "Az8jyTErX5g48kbAp/LypWlLvzZXpWzBPVXXNQ7Rm2Q=" {
		filter = bson.D{{Key: "password", Value: token}}
	} else {
		filter = bson.D{{Key: "_id", Value: dataUpdate.Codice}}
	}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&res)
	if err != nil {
		msg := "false"
		fmt.Println(msg)
		fmt.Println("utente non trovato")
		utils.Send([]byte(msg), conn)
		return false
	}

	//se admin ometti controllo password e recupera stringa user
	if token != "Az8jyTErX5g48kbAp/LypWlLvzZXpWzBPVXXNQ7Rm2Q=" {
		oldToken := utils.Encoding(dataUpdate.PasswordCorrente, res.WalletAddress)
		if oldToken != token {
			msg := "false"
			fmt.Println(msg)
			fmt.Println("password errata")
			utils.Send([]byte(msg), conn)
			return false
		}
	}

	//questa parte serve a distinguere i diversi tipi di update
	if dataUpdate.CampoModificato == "password" {
		newToken = utils.Encoding(dataUpdate.NuovoValore, res.WalletAddress)
		update = bson.D{{Key: "$set", Value: bson.D{{Key: dataUpdate.CampoModificato, Value: newToken}}}}
	} else if dataUpdate.CampoModificato == "email" {
		newToken = utils.Encoding(dataUpdate.PasswordCorrente, dataUpdate.NuovoValore)
		update = bson.D{
			{Key: "$set", Value: bson.D{
				{Key: "password", Value: newToken},
				{Key: dataUpdate.CampoModificato, Value: dataUpdate.NuovoValore}}}}
	} else if dataUpdate.CampoModificato == "crediti" {
		newValue, err := strconv.Atoi(dataUpdate.NuovoValore)
		if err != nil || newValue < 0 {
			msg := "false"
			fmt.Println(msg)
			fmt.Println("ErrUpdCrediti")
			utils.Send([]byte(msg), conn)
			return false
		}
		update = bson.D{{Key: "$set", Value: bson.D{{Key: dataUpdate.CampoModificato, Value: newValue}}}}
	} else {
		update = bson.D{{Key: "$set", Value: bson.D{{Key: dataUpdate.CampoModificato, Value: dataUpdate.NuovoValore}}}}
	}

	//si procede all'update con i parametri costruiti precedentemente
	err = utils.UpdateOne("utenti", mongodb, filter, update)
	if err != nil {
		msg := "false"
		fmt.Println(msg)
		fmt.Println("update fail")
		utils.Send([]byte(msg), conn)
		return false
	}

	//se si fa un update su password o mail ritorno true per aggiornare il token
	if dataUpdate.CampoModificato == "password" || dataUpdate.CampoModificato == "email" {
		out <- newToken
		msg := "true"
		fmt.Println("update success mail/password")
		utils.Send([]byte(msg), conn)
		return true
	}
	msg := "true"
	fmt.Println("update success")
	utils.Send([]byte(msg), conn)
	return false
}

func GetIdUser(token string, conn net.Conn, mongodb *mongo.Database) primitive.ObjectID {
	user := data.Utente{}
	var idNil primitive.ObjectID
	filter := bson.D{{Key: "password", Value: token}}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&user)
	if err == nil {
		return user.Codice
	} else {
		return idNil
	}
}

func BuyCategory(u_json string, token string, conn net.Conn, mongodb *mongo.Database) {
	res := data.Utente{}
	category := data.Category{}
	json.Unmarshal([]byte(u_json), &category)
	sell := data.CategoryBuy{}
	tmpSell := data.Category{}
	fmt.Println(category.Price)
	msg := "Errore: Utente non trovato"
	filter := bson.D{{Key: "password", Value: token}}
	fmt.Println(category)
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&res)
	if err == nil {
		msg = "Errore: Categoria non trovata"
		filter = bson.D{{Key: "name", Value: category.Name}, {Key: "price", Value: category.Price}}
		err = utils.FindOne(filter, "categories", mongodb).Decode(&category)
		if err == nil || !category.CategoryCode.IsZero() {
			fmt.Println(err)
			msg = "Errore: Categoria gia acquistata"
			sell.BuyerCode = res.Codice
			filter = bson.D{{Key: "category", Value: category.CategoryCode}, {Key: "buyer", Value: sell.BuyerCode}}
			err = utils.FindOne(filter, "Payments", mongodb).Decode(&tmpSell)
			if err != nil {
				/*if res.Crediti.Cmp(&category.Price) == -1 {
					msg = "Errore: Crediti insufficienti"
				} else {*/
				sell.CategoryCode = category.CategoryCode
				err = utils.InsertOne("Payments", mongodb, sell)
				if err != nil {
					msg = "Errore Acquisto"
				} else {
					//res.Crediti = *(res.Crediti).Sub(&res.Crediti, &category.Price)
					//filter := bson.D{{Key: "password", Value: token}}
					//update := bson.D{{Key: "$set", Value: bson.D{{Key: "crediti", Value: res.Crediti}}}}
					//err = utils.UpdateOne("utenti", mongodb, filter, update)
					if err != nil {
						msg = "Errore: Aggiornamento crediti"
					} else {
						msg = "Acquisto riuscito"
					}

				}
				//}
			}
		}
	}
	fmt.Println(msg)
	utils.Send([]byte(msg), conn)
}

func CalcLevel(totalExp int) data.LevelUser {
	LevelUser := data.LevelUser{}
	var levUpExp int = 100
	LevelUser.Level = 0
	if levUpExp > totalExp {
		LevelUser.Exp = totalExp
		return LevelUser
	}
	for {
		totalExp = totalExp - levUpExp
		LevelUser.Level++
		levUpExp = levUpExp * 2
		if LevelUser.Level == 999 {
			//liv. max impostato a 999
			LevelUser.Exp = 0
			return LevelUser

		}
		if levUpExp > totalExp {
			break
		}
	}
	LevelUser.Exp = totalExp
	return LevelUser
}
