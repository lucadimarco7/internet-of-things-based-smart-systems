package service

import (
	"Server/data"
	"Server/tokenServices"
	"Server/utils"
	"encoding/json"
	"fmt"
	"net"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func EndGameOnline(notmyscore int, myscore int, token string, score data.EndGameDataRcv, conn net.Conn, mongodb *mongo.Database) {
	dataEndGameSnd := data.EndGameDataSnd{}
	user := data.Utente{}
	filter := bson.D{{Key: "password", Value: token}}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&user)
	if err != nil {
		dataEndGameSnd.ResUpdate = false
		json_comp, _ := json.Marshal(dataEndGameSnd)
		utils.Send(json_comp, conn)
	} else {
		var token int64 = 0
		if notmyscore > myscore {
			token = 0
			dataEndGameSnd.ChallengeString += "Hai perso: Non hai guadagnato nessun token\n"
			dataEndGameSnd.ChallengeString += "Punteggio avversario: " + strconv.FormatUint(uint64(notmyscore), 10) + " \n "
		} else if notmyscore < myscore {
			token = 8
			dataEndGameSnd.ChallengeString += "Hai vinto: Hai guadagnato " + strconv.FormatInt(token, 10) + " token \n "
			dataEndGameSnd.ChallengeString += "Punteggio avversario: " + strconv.FormatUint(uint64(notmyscore), 10) + " \n "
		} else {
			token = 4
			dataEndGameSnd.ChallengeString += "Hai pareggiato: Hai guadagnato " + strconv.FormatInt(token, 10) + " token \n "
			dataEndGameSnd.ChallengeString += "Punteggio avversario: " + strconv.FormatUint(uint64(notmyscore), 10) + " \n "
		}

		if notmyscore <= myscore {
			wallet_address := user.WalletAddress
			hash := tokenServices.Transfer(wallet_address, token)
			dataEndGameSnd.ChallengeString += "Hash Transazione: " + hash
		}
		dataEndGameSnd.ResUpdate = true
		json_comp, _ := json.Marshal(dataEndGameSnd)
		fmt.Println(dataEndGameSnd)
		utils.Send(json_comp, conn)
	}
}

func EndGame(token string, u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataEndGameRcv := data.EndGameDataRcv{}
	dataEndGameSnd := data.EndGameDataSnd{}
	user := data.Utente{}
	json.Unmarshal([]byte(u_json), &dataEndGameRcv)
	filter := bson.D{{Key: "password", Value: token}}
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&user)
	if err != nil {
		dataEndGameSnd.ResUpdate = false
		json_comp, _ := json.Marshal(dataEndGameSnd)
		utils.Send(json_comp, conn)
	} else {
		//calcolare livello e exp  tramite una funzione
		exp := [7]int{0, 0, 0, 0, 0, 0, 25}
		//calcolo Exp accumulata
		if dataEndGameRcv.Game != "im" {
			dataEndGameSnd.ChallengeString = "Completa la partita: " + strconv.FormatUint(uint64(exp[6]), 10) + " exp \n "
			if dataEndGameRcv.Score >= 10 {
				exp[0] = 10
				dataEndGameSnd.ChallengeString += ("Raggiungi uno score di 10 punti: " + strconv.FormatUint(uint64(exp[0]), 10) + " exp \n ")
			}
			if dataEndGameRcv.Score >= 20 {
				exp[1] = 20
				dataEndGameSnd.ChallengeString += "Raggiungi uno score di 20 punti: " + strconv.FormatUint(uint64(exp[1]), 10) + " exp \n "
			}
			//challenge disponibile solo per sfide a tempo e classiche per via degli aiuti
			if dataEndGameRcv.Game == "cl" || dataEndGameRcv.Game == "tm" {
				if dataEndGameRcv.Score >= 30 && dataEndGameRcv.UsedHelps != 0 {
					exp[2] = 60 / dataEndGameRcv.UsedHelps
					dataEndGameSnd.ChallengeString += "Partita eccellente: " + strconv.FormatUint(uint64(exp[2]), 10) + " exp \n "
				}
				if dataEndGameRcv.Score == 30 && dataEndGameRcv.UsedHelps == 0 {
					exp[3] = 100
					dataEndGameSnd.ChallengeString += "Partita perfetta: " + strconv.FormatUint(uint64(exp[3]), 10) + " exp \n "
				}
			} else {
				//disponibile solo per vero o falso
				if dataEndGameRcv.Score >= 30 {
					exp[2] = 100
					dataEndGameSnd.ChallengeString += "Maestro del vero o falso " + strconv.FormatUint(uint64(exp[2]), 10) + " exp \n "
				}
			}
			//challenge valida solo per la prova a tempo
			if dataEndGameRcv.Game == "tm" {
				if dataEndGameRcv.LongestSeries > 5 {
					exp[4] = 2 * dataEndGameRcv.LongestSeries
					dataEndGameSnd.ChallengeString += "Serie piu lunga:" + strconv.FormatUint(uint64(exp[4]), 10) + " exp \n "
				}
			}
			//I record sono disponibili solo per sfide a tempo
			if dataEndGameRcv.Game == "tm" {
				if CheckRecord(mongodb, dataEndGameRcv.Category, dataEndGameRcv.Score, user.Codice) {
					exp[5] = 50
					dataEndGameSnd.ChallengeString += "Nuovo record:" + strconv.FormatUint(uint64(exp[5]), 10) + " exp \n "
				}
			}
		} else {
			exp[6] = 0
			//se il gioco è l'impiccato gestire l'exp diversamente
			if dataEndGameRcv.UsedHelps == 5 {
				exp[0] = dataEndGameRcv.Score * 3 * dataEndGameRcv.UsedHelps
				dataEndGameSnd.ChallengeString += ("Invincibile: " + strconv.FormatUint(uint64(exp[0]), 10) + " exp \n ")
			} else if dataEndGameRcv.UsedHelps == 4 {
				exp[0] = dataEndGameRcv.Score * 2 * dataEndGameRcv.UsedHelps
				dataEndGameSnd.ChallengeString += ("Sfiorato: " + strconv.FormatUint(uint64(exp[0]), 10) + " exp \n ")
			} else if dataEndGameRcv.UsedHelps == -1 {
				exp[0] = 10
				dataEndGameRcv.Score = 0
				dataEndGameSnd.ChallengeString += ("Sconfitta: " + strconv.FormatUint(uint64(exp[0]), 10) + " exp \n ")
			} else {
				exp[0] = dataEndGameRcv.Score * 2 * dataEndGameRcv.UsedHelps
				dataEndGameSnd.ChallengeString += ("Vittoria: " + strconv.FormatUint(uint64(exp[0]), 10) + " exp \n ")
			}
		}

		var totalExp int
		//add exp to totalExp
		for i := 0; i < len(exp); i++ {
			totalExp = totalExp + exp[i]
			fmt.Println(totalExp)
		}
		newExp := totalExp + user.Exp
		update := bson.D{
			{Key: "$set", Value: bson.D{
				{Key: "exp", Value: newExp},
			}},
		}
		err := utils.UpdateOne("utenti", mongodb, filter, update)
		if err != nil {
			dataEndGameSnd.ResUpdate = false
			json_comp, _ := json.Marshal(dataEndGameSnd)
			utils.Send(json_comp, conn)
		} else {
			//se l'update va a buon fine
			//---DA SISTEMARE-------------------------------
			/*
				dataEndGameSnd.Crediti = user.Crediti
				lvlPrec := CalcLevel(user.Exp)
				dataEndGameSnd.LevelUser = CalcLevel(totalExp + user.Exp)
				if lvlPrec.Level < dataEndGameSnd.LevelUser.Level {
					dataEndGameSnd.LevelUp = true
					dataEndGameSnd.Crediti = +5
					update := bson.D{{Key: "$set", Value: bson.D{{Key: "crediti", Value: user.Crediti + 5}}}}
					err := utils.UpdateOne("utenti", mongodb, filter, update)
					if err != nil {
						//se l'update non va a buon fine
						dataEndGameSnd.ResUpdate = false
						json_comp, _ := json.Marshal(dataEndGameSnd)
						utils.Send(json_comp, conn)
						return
					}
				}
			*/
			dataEndGameSnd.ResUpdate = true
			json_comp, _ := json.Marshal(dataEndGameSnd)
			fmt.Println(dataEndGameSnd)
			utils.Send(json_comp, conn)
			UpdateStats(dataEndGameRcv, mongodb, conn)
		}
	}
}
func GetIdCat(cat string, conn net.Conn, mongodb *mongo.Database) primitive.ObjectID {
	category := data.Category{}
	var idNil primitive.ObjectID
	filter := bson.D{{Key: "name", Value: cat}}
	err := utils.FindOne(filter, "categories", mongodb).Decode(&category)
	if err == nil {
		return category.CategoryCode
	} else {
		return idNil
	}
}
func UpdateStats(dataGame data.EndGameDataRcv, mongodb *mongo.Database, conn net.Conn) {
	//aggiornare il db in base al gioco e cat
	stats := data.Stats{}
	CategoryCode := GetIdCat(dataGame.Category, conn, mongodb)

	filter := bson.D{{Key: "categorycode", Value: CategoryCode},
		{Key: "gamecode", Value: dataGame.Game}}
	err := utils.FindOne(filter, "stats", mongodb).Decode(&stats)
	if err == nil {
		//se si trova una stats si modifica il record
		mod := bson.D{{Key: "totalgame", Value: stats.TotalGame + 1},
			{Key: "totalscore", Value: stats.Totalscore + uint(dataGame.Score)}}
		update := bson.D{{Key: "$set", Value: mod}}
		err = utils.UpdateOne("stats", mongodb, filter, update)
		if err != nil {
			fmt.Println("Errore durante l'aggiornamento delle stats")
			return
		}
		fmt.Println("update delle stats")
	} else {
		stats.GameCode = dataGame.Game
		stats.CategoryCode = CategoryCode
		stats.TotalGame = 1
		stats.Totalscore = uint(dataGame.Score)
		//creo la il record della stats
		err := utils.InsertOne("stats", mongodb, stats)
		if err != nil {
			fmt.Println("Errore durante la creazione delle stats")
			return
		}
		fmt.Println("creazione delle stats")
	}
}

func CheckRecord(mongodb *mongo.Database, category string, score int, usercode primitive.ObjectID) bool {
	record := data.Record{}
	result := data.Category{}
	//recupero categorycode
	filter := bson.D{{Key: "name", Value: category}}
	err := utils.FindOne(filter, "categories", mongodb).Decode(&result)
	if err == nil {
		filter = bson.D{
			{Key: "usercode", Value: usercode},
			{Key: "categorycode", Value: result.CategoryCode}}
		err := utils.FindOne(filter, "records", mongodb).Decode(&record)
		if err == nil {
			//se si trova un record con punteggio inferiore si modifica il record
			if score > record.Score {
				mod := bson.D{{Key: "score", Value: score}}
				update := bson.D{{Key: "$set", Value: mod}}
				err = utils.UpdateOne("records", mongodb, filter, update)
				if err == nil {
					return true
				}
			}
		} else {
			record.CategoryCode = result.CategoryCode
			record.UserCode = usercode
			record.Score = score
			//se non è stato trovato nulla inserisco il record
			err = utils.InsertOne("records", mongodb, record)
			if err == nil {
				return true
			}
		}
	}
	return false
}

func SendCategories(u_json string, token string, conn net.Conn, mongodb *mongo.Database) {
	request := data.Request{}
	sell := []data.CategoryBuy{}
	json.Unmarshal([]byte(u_json), &request)
	filter := bson.D{}
	categories := []data.Category{}
	categoriesSell := []data.Category{}
	if request.TypeRequest == "game" {
		tmp := GetIdUser(token, conn, mongodb)
		if tmp.IsZero() {
			msg := ("utente non trovato")
			utils.Send([]byte(msg), conn)
			return
		}
		filter = bson.D{{Key: "buyer", Value: tmp}}
		utils.FindAll("Payments", mongodb, filter, &sell)

		for i := 0; i < len(sell); i++ {
			tmpCatSell := data.Category{}
			filter = bson.D{{Key: "_id", Value: sell[i].CategoryCode}}
			err := utils.FindOne(filter, "categories", mongodb).Decode(&tmpCatSell)
			if err != nil {
				msg := ("ErrDB")
				utils.Send([]byte(msg), conn)
				return
			}
			fmt.Println(tmpCatSell)
			categoriesSell = append(categoriesSell, tmpCatSell)
		}
		filter = bson.D{{Key: "price", Value: 0}}
	}

	/* 	if request.TypeRequest == "update" {
	   		//non faccio nulla perchè il filtro è gia vuoto
	   	}
	*/
	if request.TypeRequest == "store" {

		tmp := GetIdUser(token, conn, mongodb)
		if tmp.IsZero() {
			msg := ("utente non trovato")
			utils.Send([]byte(msg), conn)
			return
		}
		filterAg := bson.A{
			bson.D{
				{Key: "$lookup",
					Value: bson.D{
						{Key: "from", Value: "Payments"},
						{Key: "localField", Value: "_id"},
						{Key: "foreignField", Value: "category"},
						{Key: "as", Value: "Names"},
					},
				},
			},
			bson.D{
				{Key: "$match",
					Value: bson.D{
						{Key: "Names.buyer", Value: bson.D{{Key: "$ne", Value: tmp}}},
						{Key: "price", Value: bson.D{{Key: "$gt", Value: 0}}},
					},
				},
			},
		}
		utils.Aggregate("categories", mongodb, filterAg, &categories)
	}
	if request.TypeRequest == "update" || request.TypeRequest == "game" {
		utils.FindAll("categories", mongodb, filter, &categories)
		categories = append(categories, categoriesSell...)
	}
	json_comp, _ := json.Marshal(categories)
	fmt.Println(categories)
	utils.Send(json_comp, conn)
}

func SendQuiz(u_json string, conn net.Conn, mongodb *mongo.Database) {
	questions := data.QuestionsRequest{}
	json.Unmarshal([]byte(u_json), &questions)
	//verifico che la categoria sia letta correttamente
	comp := getQuestions(questions.Game, questions.Categoria, conn, mongodb)
	fmt.Printf("comp: %v\n", comp)
	json_comp, _ := json.Marshal(comp)
	utils.Send(json_comp, conn)
}

func SendQuestions(u_json string, conn net.Conn, mongodb *mongo.Database) {
	questionsFilter := data.Question{}
	json.Unmarshal([]byte(u_json), &questionsFilter)
	res := []data.Question{}
	filter := bson.D{
		{Key: "categoria", Value: bson.D{{Key: "$regex", Value: questionsFilter.Categoria}, {Key: "$options", Value: "i"}}},
		{Key: "testo", Value: bson.D{{Key: "$regex", Value: questionsFilter.Testo}, {Key: "$options", Value: "i"}}}}
	utils.FindAll("questions", mongodb, filter, &res)
	fmt.Printf("comp: %v\n", res)
	json_comp, _ := json.Marshal(res)
	utils.Send(json_comp, conn)
}

func getQuestions(game string, categoria string, conn net.Conn, mongodb *mongo.Database) []data.Question {
	comp := []data.Question{}
	var size bson.D
	//volendo si puo inserire un intero nel messaggio ricevuto che indica quante domande estrarre
	if game != "im" {
		size = bson.D{{Key: "$sample", Value: bson.D{{Key: "size", Value: 31}}}}
	} else {
		size = bson.D{{Key: "$sample", Value: bson.D{{Key: "size", Value: 1}}}}
	}
	pipe := bson.A{
		bson.D{
			{Key: "$match",
				Value: bson.D{
					{Key: "categoria", Value: categoria},
				},
			},
		},
		size,
	}
	utils.Aggregate("questions", mongodb, pipe, &comp)
	return comp
}

func MultQuestion(conn net.Conn, mongodb *mongo.Database) []data.Question {
	comp := []data.Question{}
	var size bson.D = bson.D{{Key: "$sample", Value: bson.D{{Key: "size", Value: 31}}}}

	pipe := bson.A{
		bson.D{
			{Key: "$match",
				Value: bson.D{
					{Key: "categoria", Value: "Geografia"},
				},
			},
		},
		size,
	}
	utils.Aggregate("questions", mongodb, pipe, &comp)
	return comp
}
