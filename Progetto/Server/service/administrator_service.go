package service

import (
	"Server/data"
	"Server/utils"
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"net"
	"strconv"
)

func DeleteCategories(u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataDel := data.Update{}
	CategoryDel := data.Category{}
	json.Unmarshal([]byte(u_json), &dataDel)
	filter := bson.D{{Key: "_id", Value: dataDel.Codice}}
	msg := "Categoria non trovata"
	err := utils.FindOne(filter, "categories", mongodb).Decode(&CategoryDel)
	if err == nil {
		err = utils.DeleteOne("categories", mongodb, filter)
		msg = "Errore durante l'eliminazione della categoria"
		if err == nil {
			msg = "Categoria elimanata, Errore durante l'eliminazione delle domande associate"
			filter = bson.D{{Key: "categoria", Value: CategoryDel.Name}}
			err := utils.DeleteMany("questions", mongodb, filter)
			if err == nil {
				msg = "Success"
			}
		}
	}
	utils.Send([]byte(msg), conn)
}
func DeleteUser(u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataDel := data.Update{}
	UserDel := data.Utente{}
	json.Unmarshal([]byte(u_json), &dataDel)
	filter := bson.D{{Key: "_id", Value: dataDel.Codice}}
	msg := "L'utente selezionato è un admin o non esiste"
	err := utils.FindOne(filter, "utenti", mongodb).Decode(&UserDel)
	if err == nil && UserDel.Password != "Az8jyTErX5g48kbAp/LypWlLvzZXpWzBPVXXNQ7Rm2Q=" {
		err = utils.DeleteOne("utenti", mongodb, filter)
		msg = "Errore durante l'eliminazione dell'utente"
		if err == nil {
			msg = "Utente eliminato. \n Errore durante l'eliminazione dei pagamenti dell'utente"
			filter = bson.D{{Key: "buyer", Value: UserDel.Codice}}
			err := utils.DeleteMany("Payments", mongodb, filter)
			if err == nil {
				msg = "Success"
			}
		}
	}
	utils.Send([]byte(msg), conn)
}

func UpdateCategories(u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataUpdate := data.Update{}
	result := data.Category{}
	json.Unmarshal([]byte(u_json), &dataUpdate)
	//questa parte serve a verificare che la domanda sia presente nel db perchè potrebbe capitare che un altro admin
	// elimini la domanda da modificare nello stesso momento della modifica
	filter := bson.D{{Key: "_id", Value: dataUpdate.Codice}}
	err := utils.FindOne(filter, "categories", mongodb).Decode(&result)
	msg := "Errore, categoria non trovata"
	if err == nil {
		msg = "Success"
		mod := bson.D{{Key: dataUpdate.CampoModificato, Value: dataUpdate.NuovoValore}}
		if dataUpdate.CampoModificato == "price" {
			Price, err := strconv.Atoi(dataUpdate.NuovoValore)
			if err != nil || Price < 0 {
				msg = "Errore, prezzo non valido"
				utils.Send([]byte(msg), conn)
				return
			} else {
				mod = bson.D{{Key: dataUpdate.CampoModificato, Value: Price}}
			}

		}
		msg = "Errore durante la modifica del prezzo"
		update := bson.D{{Key: "$set", Value: mod}}
		err = utils.UpdateOne("categories", mongodb, filter, update)
		if err == nil {
			{
				msg = "Success"
				if dataUpdate.CampoModificato == "name" {
					mod := bson.D{{Key: "categoria", Value: dataUpdate.NuovoValore}}
					update := bson.D{{Key: "$set", Value: mod}}
					filter := bson.D{{Key: "categoria", Value: result.Name}}
					err = utils.UpdateMany("questions", mongodb, filter, update)
					if err != nil {
						msg = "Nome della categoria modifica, ma il campo delle domande deve essere modificato manualmente a causa di un errore di sistema"
					}
				}
			}
		}
	}
	utils.Send([]byte(msg), conn)
}

func SendProfile(u_json string, token string, conn net.Conn, mongodb *mongo.Database) {
	request := data.Request{}
	json.Unmarshal([]byte(u_json), &request)
	dataSend := data.Utente{}
	if request.TypeRequest == "one" {
		filter := bson.D{{Key: "password", Value: token}}
		err := utils.FindOne(filter, "utenti", mongodb).Decode(&dataSend)
		if err != nil {
			msg := "Errore: utente non trovato"
			utils.Send([]byte(msg), conn)
			return
		}
		json_comp, _ := json.Marshal(dataSend)
		utils.Send(json_comp, conn)
		return

	} else if request.TypeRequest == "all" {
		dataSendAll := []data.Utente{}
		filter := bson.D{}
		utils.FindAll("utenti", mongodb, filter, &dataSendAll)
		json_comp, _ := json.Marshal(dataSendAll)
		utils.Send(json_comp, conn)
		return
	} else {
		msg := "Errore: Richiesta non valida"
		utils.Send([]byte(msg), conn)
	}
}

func UpdateQuestion(u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataUpdate := data.Update{}
	var result bson.D
	json.Unmarshal([]byte(u_json), &dataUpdate)
	//questa parte serve a verificare che la domanda sia presente nel db perchè potrebbe capitare che un altro admin
	// elimini la domanda da modificare nello stsso momento della modifica
	filter := bson.D{{Key: "_id", Value: dataUpdate.Codice}}
	err := utils.FindOne(filter, "questions", mongodb).Decode(&result)
	if err != nil {
		msg := "UpdErr"
		utils.Send([]byte(msg), conn)
		return
	}
	update := bson.D{{Key: "$set", Value: bson.D{{Key: dataUpdate.CampoModificato, Value: dataUpdate.NuovoValore}}}}
	err = utils.UpdateOne("questions", mongodb, filter, update)
	if err != nil {
		msg := "UpdErr"
		utils.Send([]byte(msg), conn)
		return
	}
	msg := "Success"
	utils.Send([]byte(msg), conn)
}

func DeleteQuestion(u_json string, conn net.Conn, mongodb *mongo.Database) {
	dataUpdate := data.Question{}
	json.Unmarshal([]byte(u_json), &dataUpdate)
	filter := bson.D{{Key: "_id", Value: dataUpdate.Codice}}
	err := utils.DeleteOne("questions", mongodb, filter)
	if err != nil {
		msg := "DelErr"
		utils.Send([]byte(msg), conn)
	}
	msg := "Success"
	utils.Send([]byte(msg), conn)
}

func InsertQuestion(u_json string, conn net.Conn, mongodb *mongo.Database) {
	var result bson.D
	question := data.QuestionInsert{}
	json.Unmarshal([]byte(u_json), &question)
	//creazione filtro
	filter := bson.D{
		{Key: "testo", Value: question.Testo},
		{Key: "categoria", Value: question.Categoria},
		{Key: "risposta", Value: question.Risposta},
		{Key: "risposta2", Value: question.Risposta2},
		{Key: "risposta3", Value: question.Risposta3},
		{Key: "rispostaCorretta", Value: question.RispostaCorretta}}
	//cerco la domanda nel database
	err := utils.FindOne(filter, "questions", mongodb).Decode(&result)
	/*
		Se l'errore è nil vuol dire che la domanda inserita è presente nel database
	*/
	if err != nil {
		//si puo inserire la question
		err = utils.InsertOne("questions", mongodb, question)
		if err != nil {
			msg := "Errore durante l'inserimento della domanda"
			utils.Send([]byte(msg), conn)
			return
		}
		//se la categoria non e' presente nel database
		//aggiungiamo questa alla collection se e solo se ci sono almeno 31 domande per quella categoria
		filter = bson.D{{Key: "name", Value: question.Categoria}}
		err = utils.FindOne(filter, "categories", mongodb).Decode(&result)
		//se non la trovo in collection verifico se ci sono 31 domande per poter aggiungere la categoria
		if err != nil {
			res := []data.Question{}
			filter = bson.D{{Key: "categoria", Value: question.Categoria}}
			utils.FindAll("questions", mongodb, filter, &res)
			if len(res) >= 3 {
				cat := data.CategorySign{}
				cat.Name = question.Categoria
				cat.Price = 50
				err = utils.InsertOne("categories", mongodb, cat)
				if err != nil {
					msg := "La domanda è stata inserita con successo, " +
						"ma ci sono stati degli errori durante l'inserimento della nuova categoria, si consiglia di riprovare manualmente " +
						"a inserire la categoria manualmente, contattare l'amministratore del database:" + cat.Name
					utils.Send([]byte(msg), conn)
					return
				}
				msg := "La domanda e la categoria " + cat.Name + " sono stati inseriti con successo"
				utils.Send([]byte(msg), conn)
				return
			}
		}
		msg := "La domanda è stata inserita con successo"
		utils.Send([]byte(msg), conn)
	} else {
		msg := "La domanda che si sta provando a inserire è gia presente nel database"
		utils.Send([]byte(msg), conn)
	}
}
