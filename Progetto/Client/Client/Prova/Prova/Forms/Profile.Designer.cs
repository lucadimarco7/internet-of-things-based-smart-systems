﻿namespace Prova.Forms
{
    partial class Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblToken = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtWallet = new System.Windows.Forms.TextBox();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.btnEditUsername = new System.Windows.Forms.Button();
            this.btnEditPassword = new System.Windows.Forms.Button();
            this.lblWei = new System.Windows.Forms.Label();
            this.txtWei = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(40, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Utente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(40, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Wallet Address";
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblToken.ForeColor = System.Drawing.Color.Black;
            this.lblToken.Location = new System.Drawing.Point(40, 223);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(46, 17);
            this.lblToken.TabIndex = 9;
            this.lblToken.Text = "Token";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(40, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsername.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsername.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUsername.ForeColor = System.Drawing.Color.Black;
            this.txtUsername.Location = new System.Drawing.Point(143, 33);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(164, 21);
            this.txtUsername.TabIndex = 1;
            // 
            // txtWallet
            // 
            this.txtWallet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWallet.BackColor = System.Drawing.SystemColors.Window;
            this.txtWallet.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtWallet.ForeColor = System.Drawing.Color.Black;
            this.txtWallet.Location = new System.Drawing.Point(143, 74);
            this.txtWallet.Name = "txtWallet";
            this.txtWallet.ReadOnly = true;
            this.txtWallet.Size = new System.Drawing.Size(164, 21);
            this.txtWallet.TabIndex = 4;
            // 
            // txtToken
            // 
            this.txtToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtToken.BackColor = System.Drawing.SystemColors.Window;
            this.txtToken.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtToken.ForeColor = System.Drawing.Color.Black;
            this.txtToken.Location = new System.Drawing.Point(143, 219);
            this.txtToken.Name = "txtToken";
            this.txtToken.ReadOnly = true;
            this.txtToken.Size = new System.Drawing.Size(164, 21);
            this.txtToken.TabIndex = 10;
            // 
            // btnEditUsername
            // 
            this.btnEditUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditUsername.BackgroundImage = global::QuizApp.Properties.Resources.edit;
            this.btnEditUsername.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEditUsername.Location = new System.Drawing.Point(313, 26);
            this.btnEditUsername.Name = "btnEditUsername";
            this.btnEditUsername.Size = new System.Drawing.Size(35, 35);
            this.btnEditUsername.TabIndex = 2;
            this.btnEditUsername.UseVisualStyleBackColor = true;
            this.btnEditUsername.Click += new System.EventHandler(this.BtnEditUsername_Click);
            // 
            // btnEditPassword
            // 
            this.btnEditPassword.BackgroundImage = global::QuizApp.Properties.Resources.edit;
            this.btnEditPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEditPassword.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEditPassword.ForeColor = System.Drawing.Color.Black;
            this.btnEditPassword.Location = new System.Drawing.Point(143, 116);
            this.btnEditPassword.Name = "btnEditPassword";
            this.btnEditPassword.Size = new System.Drawing.Size(35, 35);
            this.btnEditPassword.TabIndex = 6;
            this.btnEditPassword.UseVisualStyleBackColor = true;
            this.btnEditPassword.Click += new System.EventHandler(this.BtnEditPassword_Click);
            // 
            // lblWei
            // 
            this.lblWei.AutoSize = true;
            this.lblWei.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblWei.Location = new System.Drawing.Point(40, 175);
            this.lblWei.Name = "lblWei";
            this.lblWei.Size = new System.Drawing.Size(34, 17);
            this.lblWei.TabIndex = 7;
            this.lblWei.Text = "Wei";
            // 
            // txtWei
            // 
            this.txtWei.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWei.BackColor = System.Drawing.SystemColors.Window;
            this.txtWei.ForeColor = System.Drawing.Color.Black;
            this.txtWei.Location = new System.Drawing.Point(143, 174);
            this.txtWei.Name = "txtWei";
            this.txtWei.ReadOnly = true;
            this.txtWei.Size = new System.Drawing.Size(164, 23);
            this.txtWei.TabIndex = 8;
            // 
            // Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(382, 272);
            this.Controls.Add(this.txtWei);
            this.Controls.Add(this.lblWei);
            this.Controls.Add(this.btnEditPassword);
            this.Controls.Add(this.btnEditUsername);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.txtWallet);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblToken);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(398, 311);
            this.Name = "Profile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.VisibleChanged += new System.EventHandler(this.Profile_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label lblToken;
        private Label label5;
        private TextBox txtUsername;
        private TextBox txtWallet;
        private TextBox txtToken;
        private Button btnEditUsername;
        private Button btnEditPassword;
        private Label lblWei;
        private TextBox txtWei;
    }
}