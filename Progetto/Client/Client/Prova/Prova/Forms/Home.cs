﻿using Prova.Connection;
using Prova.Data;
using Prova.UserControls.Game;
using System.Diagnostics;
using System.Net;
using System.Text;

namespace Prova.Forms
{
    public partial class Home : Form
    {
        private readonly Login parent;
        private readonly Protocol pt;

        public Home(Login startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void BtnBuyTokens_Click(object sender, EventArgs e)
        {
            BuyTokens frm = new(this);
            ShowForm(frm);
        }

        private void BtnProfile_Click(object sender, EventArgs e)
        {
            Profile frm = new(this);
            ShowForm(frm);
        }

        private void BtnShop_Click(object sender, EventArgs e)
        {
            WithdrawWei frm = new(this);
            ShowForm(frm);
        }

        private void BtnRankings_Click(object sender, EventArgs e)
        {
            Rankings frm = new(this);
            ShowForm(frm);
        }

        private void ShowForm(Form frm)
        {
            frm.Show();
            this.Visible = false;
        }

        private void BtnLogOut_Click(object sender, EventArgs e)
        {
            pt.SetProtocolID("logout");
            SocketTCP.Send(pt.ToString());
            parent.Visible = true;
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (!parent.Visible)
            {
                pt.SetProtocolID("close");
                SocketTCP.Send(pt.ToString());
                Application.Exit();
            }
        }

        private async void btnOnline_Click(object sender, EventArgs e)
        {
            string balanceJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        wallet_address = Singleton.Instance.User.WalletAddress
                    }
                    );
            pt.SetProtocolID("getWalletBalance");
            pt.Data = balanceJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            pt.SetProtocolID("getEthAvailability");
            SocketTCP.Send(pt.ToString());
            string response2 = SocketTCP.Receive();
            pt.SetProtocolID("hasPending");
            SocketTCP.Send(pt.ToString());
            string response3 = SocketTCP.Receive();
            Debug.WriteLine("pending: " + response3);
            if (int.Parse(response) >= 5 && response2.Equals("Eth sufficienti") && response3.Equals("false"))
            {
                //--------------------Transazione-------------------------------
                bool ok = false;
                string to = "0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727"; // inserisci qui l'importo di token da acquistare
                long value = 5; // inserisci qui l'importo di ether da inviare

                string url = $"http://localhost:8080/transfer.html?to={to}&value={value}";
                Process.Start(new ProcessStartInfo(url) { UseShellExecute = true });

                this.Hide();
                Form form = new Form();
                UCOnlineWait uCOnlineWait = new UCOnlineWait();
                uCOnlineWait.SetLabel1("Attendi conferma transazione");
                form.Controls.Add(uCOnlineWait);
                form.AutoSize = true;
                form.Show();
                form.FormClosing += new FormClosingEventHandler(form_FormClosing);


                //-----------------JS---------------------------------------
                Task myTask = Task.Run(() =>
                {
                    HttpListener listener = new HttpListener();
                    listener.Prefixes.Add("http://localhost:8080/");
                    try
                    {
                        listener.Start();
                    }
                    catch (HttpListenerException ex)
                    {
                        Console.WriteLine("Listener già avviato sulla porta 8080", ex.Message);
                    }

                    Debug.WriteLine("Server HTTP avviato");

                    while (true)
                    {

                        HttpListenerContext context = listener.GetContext();
                        HttpListenerRequest request = context.Request;
                        HttpListenerResponse response = context.Response;

                        // Gestisci le richieste GET per /prova.html
                        if (request.HttpMethod == "GET" && request.Url!.AbsolutePath == "/transfer.html")
                        {
                            // Invia il contenuto della pagina HTML come risposta
                            byte[] buffer = Encoding.UTF8.GetBytes(File.ReadAllText("transfer.html"));
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);
                        }

                        // Gestisci le richieste GET per /prova.js
                        if (request.HttpMethod == "GET" && request.Url!.AbsolutePath == "/transfer.js")
                        {
                            // Imposta il tipo MIME corretto per i file JavaScript
                            response.ContentType = "application/javascript";

                            // Invia il contenuto del file JavaScript come risposta
                            byte[] buffer = Encoding.UTF8.GetBytes(File.ReadAllText("transfer.js"));
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);
                        }

                        // Gestisci le richieste POST
                        if (request.HttpMethod == "POST")
                        {
                            // Leggi i dati inviati dal client
                            using (StreamReader reader = new StreamReader(request.InputStream))
                            {
                                string data = reader.ReadToEnd();
                                Debug.WriteLine(data);
                                if (data.Contains("OK"))
                                {
                                    MessageBox.Show("Transazione riuscita!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    listener.Stop();
                                    ok = true;
                                    return;
                                }
                                else if (data.Contains("ERROR"))
                                {
                                    MessageBox.Show("Transazione non riuscita!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    listener.Stop();
                                    ok = false;
                                    return;
                                }
                            }
                        }
                        response.Close();
                    }
                });
                await myTask;
                
                form.Close();
                this.Show();

                //--------------------------------------------------------------
                if (ok)
                {
                    Online frm = new(this);
                    ShowForm(frm);
                }
            }
            else
            {
                MessageBox.Show("Token o ETH insufficienti per avviare una partita", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        void form_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Show();
        }
    }
}
