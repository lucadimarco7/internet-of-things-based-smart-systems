﻿using Microsoft.VisualBasic;
using Prova.Utils;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class Profile : Form
    {
        private readonly Home parent;
        private readonly Protocol pt;

        public Profile(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void Profile_VisibleChanged(object sender, EventArgs e)
        {
            if (Singleton.Instance.User != null &&
                Singleton.Instance.User.LevelUser != null)
            {
                txtUsername.Text = Singleton.Instance.User.Nome;
                txtWallet.Text = Singleton.Instance.User.WalletAddress;
                string ethJson = System.Text.Json.JsonSerializer.Serialize(
                   new
                   {
                       wallet_address = Singleton.Instance.User.WalletAddress
                   }
                   );
                pt.SetProtocolID("getEth");
                pt.Data = ethJson;
                SocketTCP.Send(pt.ToString());
                txtWei.Text = SocketTCP.Receive();
                pt.SetProtocolID("getWalletBalance");
                SocketTCP.Send(pt.ToString());
                txtToken.Text = SocketTCP.Receive();
            }
            else
            {
                Debug.Print("Singleton istance user is null");
            }
        }

        private void BtnEditUsername_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Inserisci password:", "Modifica", "");
            string input2 = Interaction.InputBox("Inserisci nuovo nome utente", "Modifica", "");

            if (CheckFields.CheckUsername(input2))
            {
                string profileJson = System.Text.Json.JsonSerializer.Serialize(new
                {
                    PasswordCorrente = input,
                    CampoModificato = "nome",
                    NuovoValore = input2
                });

                pt.SetProtocolID("editProfile");
                pt.Data = profileJson;
                SocketTCP.Send(pt.ToString());
                string responseData = SocketTCP.Receive();

                if (responseData.Contains("false"))
                {
                    MessageBox.Show("Modifica fallita, ricontrolla la password.", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Singleton.Instance.User.Nome = input2;
                    MessageBox.Show("Nome Utente modificato con successo.", "Completato",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Profile_VisibleChanged(this, e);
                }
            }
            else
            {
                MessageBox.Show("Nome Utente non valido.\n" +
                    "Puoi usare A-z, 0-9. La lunghezza minima è 5 caratteri.", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnEditPassword_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Inserisci vecchia password:", "Modifica", "");
            string input2 = Interaction.InputBox("Inserisci nuova password:", "Modifica", "");
            string input3 = Interaction.InputBox("Conferma nuova password:", "Modifica", "");
            string check = CheckFields.CheckPassword(input2, input3);

            if (check.Equals("Password valida"))
            {
                Debug.WriteLine("Username modificato, nuovo username: " + input);
                string profileJson = System.Text.Json.JsonSerializer.Serialize(new
                {
                    PasswordCorrente = input,
                    CampoModificato = "password",
                    NuovoValore = input2,
                });

                pt.SetProtocolID("editProfile");
                pt.Data = profileJson;

                SocketTCP.Send(pt.ToString());
                string responseData = SocketTCP.Receive();
                Debug.WriteLine("PASSWORD: " + responseData);
                if (responseData.Contains("false"))
                {
                    MessageBox.Show("Modifica fallita, ricontrolla la vecchia password", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Password modificata con successo.", "Completato",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show(check, "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            parent.Visible = true;
        }
    }
}
