﻿namespace Prova.Forms
{
    partial class WithdrawWei
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtToken = new System.Windows.Forms.TextBox();
            this.txtWei = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBuy = new System.Windows.Forms.Button();
            this.numToken = new System.Windows.Forms.NumericUpDown();
            this.txtWithdrawWei = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numToken)).BeginInit();
            this.SuspendLayout();
            // 
            // txtToken
            // 
            this.txtToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtToken.BackColor = System.Drawing.SystemColors.Window;
            this.txtToken.Location = new System.Drawing.Point(133, 46);
            this.txtToken.MinimumSize = new System.Drawing.Size(276, 23);
            this.txtToken.Name = "txtToken";
            this.txtToken.ReadOnly = true;
            this.txtToken.Size = new System.Drawing.Size(276, 23);
            this.txtToken.TabIndex = 3;
            // 
            // txtWei
            // 
            this.txtWei.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWei.BackColor = System.Drawing.SystemColors.Window;
            this.txtWei.Location = new System.Drawing.Point(133, 6);
            this.txtWei.Name = "txtWei";
            this.txtWei.ReadOnly = true;
            this.txtWei.Size = new System.Drawing.Size(276, 23);
            this.txtWei.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Token Disponibili";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wei Disponibili";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Inserisci importo in Token";
            // 
            // btnBuy
            // 
            this.btnBuy.Location = new System.Drawing.Point(279, 112);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(102, 23);
            this.btnBuy.TabIndex = 6;
            this.btnBuy.Text = "Preleva Wei";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // numToken
            // 
            this.numToken.Location = new System.Drawing.Point(12, 112);
            this.numToken.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numToken.Name = "numToken";
            this.numToken.Size = new System.Drawing.Size(120, 23);
            this.numToken.TabIndex = 4;
            this.numToken.ValueChanged += new System.EventHandler(this.numToken_ValueChanged);
            // 
            // txtWithdrawWei
            // 
            this.txtWithdrawWei.BackColor = System.Drawing.SystemColors.Window;
            this.txtWithdrawWei.Location = new System.Drawing.Point(173, 113);
            this.txtWithdrawWei.Name = "txtWithdrawWei";
            this.txtWithdrawWei.ReadOnly = true;
            this.txtWithdrawWei.Size = new System.Drawing.Size(100, 23);
            this.txtWithdrawWei.TabIndex = 5;
            this.txtWithdrawWei.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "L\'importo minimo in Token è 10";
            // 
            // WithdrawWei
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(421, 203);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtWithdrawWei);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.numToken);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.txtWei);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(437, 242);
            this.Name = "WithdrawWei";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shop";
            this.Load += new System.EventHandler(this.Shop_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numToken)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox txtToken;
        private TextBox txtWei;
        private Label label2;
        private Label label1;
        private Label label4;
        private Button btnBuy;
        private NumericUpDown numToken;
        private TextBox txtWithdrawWei;
        private Label label3;
    }
}