﻿using Prova.UserControls.AdminHome;
using Prova.Connection;
using Prova.UserControls.Game;
using System.Diagnostics;

namespace Prova.Forms
{
    public partial class AdminHome : Form
    {
        private readonly Login parent;
        private readonly Protocol pt;

        public AdminHome(Login startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }

        private void AdminHome_Load(object sender, EventArgs e)
        {
            UCEditCategories ucEditCategories= new();
            ucEditCategories.UCEditCategoriesClicked += new EventHandler(Reload);
            tabPage1.Controls.Add(ucEditCategories);
            tabPage2.Controls.Add(new UCEditQuestions());
            UCAddQuestion ucAddQuestion = new();
            ucAddQuestion.UCAddQuestionClicked += new EventHandler(Reload);
            tabPage3.Controls.Add(ucAddQuestion);
            tabPage4.Controls.Add(new UCEditUsers());
            tabPage5.Controls.Add(new UCWithdraw());
        }
        private void Reload(object? sender, EventArgs e)
        {
            if (sender != null)
            {
                tabPage1.Controls.Clear();
                tabPage2.Controls.Clear();
                tabPage3.Controls.Clear();
                tabPage4.Controls.Clear();
                tabPage5.Controls.Clear();
                AdminHome_Load(sender, e);
            }
        }



        private void BtnLogOut_Click(object sender, EventArgs e)
        {
            pt.SetProtocolID("close");
            SocketTCP.Send(pt.ToString());
            parent.Visible = true;
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (!parent.Visible)
            {
                pt.SetProtocolID("close");
                SocketTCP.Send(pt.ToString());
                Application.Exit();
            }
        }
    }
}
