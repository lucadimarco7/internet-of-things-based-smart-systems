﻿using Prova.Connection;
using Prova.Utils;
using System.Text.Json;

namespace Prova.Forms
{
    public partial class Register : Form
    {
        private readonly Protocol pt;

        public Register()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void ShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMostraPassword.Checked)
            {
                txtPassword.PasswordChar = '\0';
                txtConfermaPassword.PasswordChar = '\0';
            }
            else
            {
                txtPassword.PasswordChar = '•';
                txtConfermaPassword.PasswordChar = '•';
            }
        }

        private void LinkLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Login frm = new();
            frm.Show();
            this.Visible = false;
        }

        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            string result = CheckFields.CheckRegister(txtWallet.Text, txtUsername.Text, txtPassword.Text, txtConfermaPassword.Text);

            switch (result)
            {
                case "Registrazione avvenuta correttamente":
                    string userJson = JsonSerializer.Serialize(
                    new
                    {
                        wallet_address = txtWallet.Text,
                        Nome = txtUsername.Text,
                        Password = txtPassword.Text
                    }
                    );
                    pt.SetProtocolID("register");
                    pt.Data = userJson;
                    SocketTCP.Send(pt.ToString());
                    string response = SocketTCP.Receive();

                    if (response.Contains("Registrazione effettuata"))
                    {
                        txtWallet.Text = string.Empty;
                        txtUsername.Text = string.Empty;
                        txtPassword.Text = string.Empty;
                        txtConfermaPassword.Text = string.Empty;
                        MessageBox.Show(result, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(response,
                           "Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    break;

                default:
                    MessageBox.Show(result, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }
        }

        private void TextKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BtnConfirm_Click(this, new EventArgs());
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            pt.SetProtocolID("close");
            SocketTCP.Send(pt.ToString());
            Application.Exit();
        }
    }
}
