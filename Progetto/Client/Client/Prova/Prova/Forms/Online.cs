﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using Prova.UserControls.Game;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using static System.Net.Mime.MediaTypeNames;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Prova.Forms
{
    public partial class Online : Form
    {
        private readonly Home parent;
        private readonly Protocol pt;

        public Online(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
        }


        private void Game_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBox.Show("1 minuto di gioco e 30 domande, vince chi indovina più risposte.\n");
        }

        protected override void OnClosed(EventArgs e)
        {
            if (this.Controls.Count > 0)
            {
                if (this.Controls[0] is Prova.UserControls.Game.UCTimeGame == false)
                {
                    pt.SetProtocolID("closeOnline");
                    SocketTCP.Send(pt.ToString());
                }
                else
                {
                    UCTimeGame uc = (UCTimeGame)this.Controls[0];
                    uc.StopTimer();
                    pt.SetProtocolID("stopGameOnline");
                    SocketTCP.Send(pt.ToString());
                }
                this.Controls.Clear();
                parent.Visible = true;
            }
            this.Dispose();
        }

        private async void Online_Load(object sender, EventArgs e)
        {
            this.Controls.Clear();
            this.Controls.Add(new UCOnlineWait());

            await RicercaAvversario(e);
        }

        private async Task RicercaAvversario(EventArgs e)
        {
            List<Question> list = new List<Question>();
            Question[]? questions;
            pt.SetProtocolID("online");
            SocketTCP.Send(pt.ToString());
            string response = "wait";
            while (response.Equals("wait"))
            {
                await Task.Delay(500);
                response = SocketTCP.Receive();
            }
            if (response.Equals("Server pieno"))
            {
                MessageBox.Show("Server Pieno, riprovare più tardi", "Errore",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                OnClosed(e);
                return;
            }
            if (response.Equals("EXIT"))
            {
                return;
            }
            if (response.Contains("Error"))
            {
                MessageBox.Show(response, "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            questions = JsonConvert.DeserializeObject<Question[]>(response);

            if (questions != null)
            {
                for (int i = 0; i < questions.Length; i++)
                {
                    list.Add(new Question()
                    {
                        Testo = questions[i].Testo,
                        Risposta = questions[i].Risposta,
                        Risposta2 = questions[i].Risposta2,
                        Risposta3 = questions[i].Risposta3,
                        RispostaCorretta = questions[i].RispostaCorretta
                    });
                }
            }
            else
            {
                Debug.Print("Questions are null");
            }

            
            this.Controls.Clear();
            this.Controls.Add(new UCTimeGame(list, "", true));
        }
    }
}
