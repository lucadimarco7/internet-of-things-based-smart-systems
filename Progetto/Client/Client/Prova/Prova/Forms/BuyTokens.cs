﻿using Prova.Connection;
using Prova.Data;
using Prova.UserControls.Game;
using System.Diagnostics;
using Jint;
using System.Net;
using System.Text;
using Nethereum.Contracts.QueryHandlers.MultiCall;

namespace Prova.Forms
{
    public partial class BuyTokens : Form
    {
        private readonly Home parent;
        private string game = string.Empty;
        private readonly Protocol pt;

        public BuyTokens(Home startingForm)
        {
            InitializeComponent();
            parent = startingForm;
            pt = new Protocol();
            
        }

        private void BuyTokens_Load(object sender, EventArgs e)
        {
            string ethJson = System.Text.Json.JsonSerializer.Serialize(
                   new
                   {
                       wallet_address = Singleton.Instance.User.WalletAddress
                   }
                   );
            pt.SetProtocolID("getEth");
            pt.Data = ethJson;
            SocketTCP.Send(pt.ToString());
            txtWei.Text = SocketTCP.Receive();
            pt.SetProtocolID("getWalletBalance");
            SocketTCP.Send(pt.ToString());
            txtToken.Text = SocketTCP.Receive();
        }

        private async void btnBuy_Click(object sender, EventArgs e)
        {
            pt.SetProtocolID("hasPending");
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            Debug.WriteLine("pending: " + response);
            if (response.Equals("true"))
            {
                MessageBox.Show("Hai delle transazioni pendenti, attendi che vengano confermate", "Attenzione",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (((double)numToken.Value*0.001) < double.Parse(txtWei.Text))
            {
                int amount = (int)numToken.Value; // inserisci qui l'importo di token da acquistare
                long value = (long)numToken.Value; // inserisci qui l'importo di wei da inviare

                string url = $"http://localhost:8080/buyTokens.html?amount={amount}&value={value}";
                Process.Start(new ProcessStartInfo(url) { UseShellExecute = true });

                this.Hide();
                Form form = new Form();
                UCOnlineWait uCOnlineWait = new UCOnlineWait();
                uCOnlineWait.SetLabel1("Attendi conferma transazione");
                form.Controls.Add(uCOnlineWait);
                form.AutoSize = true;
                form.Show();

                //-----------------JS---------------------------------------
                Task myTask = Task.Run(() =>
                {
                    HttpListener listener = new HttpListener();
                    listener.Prefixes.Add("http://localhost:8080/");
                    try
                    {
                        listener.Start();
                    }
                    catch (HttpListenerException ex)
                    {
                        Console.WriteLine("Listener già avviato sulla porta 8080", ex.Message);
                    }

                    Debug.WriteLine("Server HTTP avviato");

                    while (true)
                    {
                        
                        HttpListenerContext context = listener.GetContext();
                        HttpListenerRequest request = context.Request;
                        HttpListenerResponse response = context.Response;
                        
                        // Gestisci le richieste GET per /prova.html
                        if (request.HttpMethod == "GET" && request.Url!.AbsolutePath == "/buyTokens.html")
                        {
                            // Invia il contenuto della pagina HTML come risposta
                            byte[] buffer = Encoding.UTF8.GetBytes(File.ReadAllText("buyTokens.html"));
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);
                        }

                        // Gestisci le richieste GET per /prova.js
                        if (request.HttpMethod == "GET" && request.Url!.AbsolutePath == "/buyTokens.js")
                        {
                            // Imposta il tipo MIME corretto per i file JavaScript
                            response.ContentType = "application/javascript";

                            // Invia il contenuto del file JavaScript come risposta
                            byte[] buffer = Encoding.UTF8.GetBytes(File.ReadAllText("buyTokens.js"));
                            response.ContentLength64 = buffer.Length;
                            response.OutputStream.Write(buffer, 0, buffer.Length);
                        }

                        // Gestisci le richieste POST
                        if (request.HttpMethod == "POST")
                        {
                            // Leggi i dati inviati dal client
                            using (StreamReader reader = new StreamReader(request.InputStream))
                            {
                                string data = reader.ReadToEnd();
                                Debug.WriteLine(data);
                                if (data.Contains("OK"))
                                {
                                    MessageBox.Show("Transazione riuscita!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    listener.Stop();
                                    return;
                                }
                                else if (data.Contains("ERROR"))
                                {
                                    MessageBox.Show("Transazione non riuscita!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    listener.Stop();
                                    return;
                                }
                            }
                        }
                        response.Close();
                    }
                });
                await myTask;
                form.Close();
                BuyTokens_Load(this, e);
                this.Show();
            }
            else
            {
                MessageBox.Show("ETH insufficienti per comprare Token", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        protected override void OnClosed(EventArgs e)
        {
            parent.Visible = true;
            this.Dispose();
        }
    }
}
