﻿using Prova.Connection;
using Prova.Utils;
using System.Diagnostics;
using Prova.Data;
using Newtonsoft.Json;

namespace Prova.Forms
{
    public partial class Login : Form
    {
        private readonly Protocol pt;

        public Login()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void ShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (cbMostraPassword.Checked)
                txtPassword.PasswordChar = '\0';
            else
                txtPassword.PasswordChar = '•';
        }

        private void LinkRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Register frm = new();
            frm.Show();
            this.Visible = false;
        }

        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            string result = CheckFields.CheckLogin(txtWallet.Text, txtPassword.Text);
            switch (result)
            {
                case "Login effettuato correttamente":
                    string UserJson;

                    UserJson = System.Text.Json.JsonSerializer.Serialize(new
                    {
                        wallet_address = txtWallet.Text,
                        Password = txtPassword.Text
                    });



                    pt.SetProtocolID("login");
                    pt.Data = UserJson;
                    SocketTCP.Send(pt.ToString());
                    string responseData = SocketTCP.Receive();

                    if (responseData.Contains("false"))
                    {
                        responseData = SocketTCP.Receive();
                        Debug.WriteLine(responseData);
                        User? user = JsonConvert.DeserializeObject<User>(responseData);
                        if (user != null)
                            Singleton.Instance.User = user;
                        else
                            Debug.Print("User is null");
                    }

                    if (responseData.Contains("Errore"))
                    {
                        MessageBox.Show(responseData, "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (responseData.Contains("true"))
                    {
                        AdminHome frm = new(this);
                        frm.Show();
                        this.Visible = false;
                    }
                    else
                    {
                        Home frm = new(this);
                        frm.Show();
                        this.Visible = false;
                    }

                    break;

                default:
                    MessageBox.Show(result, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }
        }

        private void TextKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BtnConfirm_Click(this, new EventArgs());
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            pt.SetProtocolID("close");
            SocketTCP.Send(pt.ToString());
            Application.Exit();
        }
    }
}
