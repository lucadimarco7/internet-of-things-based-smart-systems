﻿namespace Prova.Utils
{
    class CheckFields
    {
        public static string CheckRegister(string walletAddress, string username, string password, string confermaPassword)
        {
            if (walletAddress != string.Empty && username != string.Empty && password != string.Empty && confermaPassword != string.Empty)
            {
                string check;
                check = CheckWalletAddress(walletAddress);

                if (check.Equals("Valid Wallet Address"))
                {
                    if (CheckUsername(username))
                    {
                        check = CheckPassword(password, confermaPassword);
                        if (check.Equals("Password valida"))
                        {
                            return "Registrazione avvenuta correttamente";
                        }
                        else
                        {
                            return check;
                        }
                    }
                    else
                    {
                        return "Name Utente non valido";
                    }
                }
                else
                {
                    return check;
                }
            }
            else
            {
                return "Riempire tutti i campi";
            }
        }

        public static string CheckLogin(string walletAddress, string password)
        {
            if (walletAddress != string.Empty && password != string.Empty)
            {
                if (walletAddress.Contains(' '))
                {
                    return "Remove spaces within the Wallet Address field";
                }
                else
                {
                    return "Login effettuato correttamente";
                }
            }
            else
            {
                return "Riempire tutti i campi";
            }
        }

        public static string CheckWalletAddress(string walletAddress)
        {
            bool isValidWalletAddress = walletAddress.Contains(' ');
            if (isValidWalletAddress)
            {
                return "Remove spaces within the Wallet Address field";
            }
            return "Valid Wallet Address";
        }

        public static bool CheckUsername(string username)
        {
            int matchCount = 0;
            for (int i = 0; i < username.Length; i++)
                if (username[i] >= 'a' && username[i] <= 'z' || username[i] >= 'A'
                    && username[i] <= 'Z' || username[i] >= '0' && username[i] <= '9')
                    matchCount++;
            if (matchCount != username.Length || username.Length < 5)
                return false;
            return true;
        }

        public static string CheckPassword(string password, string confermaPassword)
        {
            bool isValidPassword = password.Contains(' ');
            bool isValidPassword2 = true;
            if (password.Length == 0)
                isValidPassword2 = false;
            if (isValidPassword || !isValidPassword2)
            {
                return "Password non valida";
            }
            else
            {
                if (password != confermaPassword)
                {
                    return "Password non coincidenti";
                }
                else
                {
                    return "Password valida";
                }
            }
        }
    }
}
