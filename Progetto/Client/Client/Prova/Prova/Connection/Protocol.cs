﻿namespace Prova.Connection
{
    public class Protocol
    {
        private string ProtocolID { get; set; }
        public string Data { get; set; }
        public string Limit;
        public string End;
        private readonly Dictionary<string, string> FuncDict;

        public Protocol()
        {
            FuncDict = new Dictionary<string, string>
        {
            { "register","0" },{ "login","1" },{ "game","2" },{ "score","3" },
                { "questions", "4" },{ "addQuestions", "5" },{ "searchQuestions", "6" },
                { "editQuestion", "7" },{ "deleteQuestion", "8" },{ "editProfile", "9" },
                { "buyCategory", "10" },{ "editCategories", "11" },{ "deleteCategories", "12" },
                { "users", "13" },{ "deleteUsers", "14" },{ "stats", "15" },
                { "close","16" },{ "online","17" },{ "closeOnline","18" },{ "scoreOnline","19" },
                { "stopGameOnline","20" },{ "logout","21" },{"getContractBalance","22"},
                {"withdrawTokens","23"},{"withdrawEth","24"},{"getWalletBalance","25"},
                {"getEthAvailability","26"},{"hasPending","27"},{"getEth","28"},{"buyTokens","29"}
        };
            ProtocolID = String.Empty;
            Data = String.Empty;
            Limit = " ";
            End = "\n";
        }
        public void SetProtocolID(string NameFunction)
        {
            ProtocolID = FuncDict[NameFunction];
        }
        public override string ToString()
        {
            return ProtocolID + Limit + Data + End;
        }
    }
}
