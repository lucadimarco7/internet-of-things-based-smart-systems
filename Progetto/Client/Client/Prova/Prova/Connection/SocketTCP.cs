﻿using System.Diagnostics;
using System.Drawing;
using System.Net.Sockets;
using System.Text;

namespace Prova.Connection
{
    internal class SocketTCP
    {
        private static readonly Mutex mut;
        private static readonly TcpClient client;
        const string host = "localhost";
        const Int32 port = 13000;
        const Int32 timeout = 10000;
        private static readonly NetworkStream stream;

        static SocketTCP()
        {
            mut = new Mutex();
            client = new TcpClient();

            try
            {
                client.Connect(host, port);
                stream = client.GetStream();
                stream.ReadTimeout = timeout;
            }
            catch (SocketException e)
            {
                MessageBox.Show(e.Message + "Assicurarsi che il server remoto sia in ascolto");
                System.Environment.Exit(50);
            }
        }
        static public void Send(string message)
        {
            Debug.WriteLine("Messaggio inviato: ", message); 
            byte[] outJson = Encoding.ASCII.GetBytes(message);
            var lenbytes = new byte[16];
            string lenmsg = Convert.ToString(outJson.Length);
            byte[] len_ = Encoding.ASCII.GetBytes(lenmsg);
            var difference = lenbytes.Length - len_.Length;

            for (int i = 0; i < len_.Length; i++)
            {
                lenbytes[difference + i - 1] = len_[i];
            }
            lenbytes[lenbytes.Length - 1] = 10;

            if (client.Connected && stream.CanWrite)
            {
                try
                {
                    stream.Write(lenbytes, 0, lenbytes.Length);
                    stream.Flush();
                    stream.Write(outJson, 0, outJson.Length);
                    stream.Flush();
                }
                catch (IOException io)
                {
                    MessageBox.Show(io.Message + "Connessione col Server interrotta");
                    System.Environment.Exit(51);
                }
            }
        }

        static public string Receive()
        {
            var data = new Byte[16];
            String responseData = String.Empty;
            if (client.Connected && stream.CanRead)
            {
                try
                {
                    Int32 bytes = stream.Read(data, 0, data.Length);
                    int inizio = 0;
                    for (int i = 0; i < 16; i++)
                    {
                        if (data[i] > 47 && data[i] < 58)
                        {
                            inizio = i;
                            break;
                        }
                    }
                    int fine = data.Length - 1;
                    data = data[inizio..fine];
                    string lenData = System.Text.Encoding.ASCII.GetString(data, 0, data.Length);
                    Debug.WriteLine("lenData: " + lenData);
                    int lenmsg = int.Parse(lenData);
                    Debug.WriteLine("lenmsg: " + lenmsg);
                    var msg = new Byte[lenmsg];
                    var total = 0;
                    do
                    {
                        var read = stream.Read(msg, total, msg.Length - total);
                        total += read;
                        Debug.WriteLine("Client recieved {0} bytes "+ total);
                    } while (total != msg.Length);

                    responseData = System.Text.UTF8Encoding.UTF8.GetString(msg, 0, msg.Length);
                    Debug.WriteLine("Data Length: "+data.Length);
                    Debug.WriteLine("Received: {0}", responseData);
                }
                catch (IOException io)
                {
                    MessageBox.Show(io.Message + "Connessione col Server interrotta");
                    Protocol pt = new Protocol();
                    pt.SetProtocolID("close");
                    SocketTCP.Send(pt.ToString());
                    System.Environment.Exit(51);
                }
            }
            return responseData;
        }
    }

}
