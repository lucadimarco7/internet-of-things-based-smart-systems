﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCWithdraw : UserControl
    {
        private readonly Protocol pt;
        private Category[]? categories;
        private string stats = String.Empty;

        public UCWithdraw()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCStats_Load(object sender, EventArgs e)
        {
            pt.SetProtocolID("getContractBalance");
            SocketTCP.Send(pt.ToString());
            txtTokenBalance.Text = SocketTCP.Receive();
            string ethJson = System.Text.Json.JsonSerializer.Serialize(
                   new
                   {
                       wallet_address = "0xaFfeB2f6b7a172801b407FcCD3BE04940EF8a727"
                   }
                   );
            pt.SetProtocolID("getEth");
            pt.Data = ethJson;
            SocketTCP.Send(pt.ToString());
            txtETHBalance.Text = SocketTCP.Receive();
        }

        private void btnWithdraw_Click(object sender, EventArgs e)
        {
            string withdrawJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        amount = numWithdrawToken.Value
                    }
                    );
            pt.SetProtocolID("withdrawTokens");
            pt.Data = withdrawJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            MessageBox.Show(response, "Prelievo",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            UCStats_Load(sender, e);
        }

        private void btnWithdrawETH_Click(object sender, EventArgs e)
        {
            string withdrawJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        amount = numWithdrawWei.Value
                    }
                    );
            pt.SetProtocolID("withdrawEth");
            pt.Data = withdrawJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            MessageBox.Show(response, "Prelievo",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            UCStats_Load(sender, e);
        }
    }
}
