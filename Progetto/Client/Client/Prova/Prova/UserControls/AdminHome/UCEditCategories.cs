﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCEditCategories : UserControl
    {
        private readonly Protocol pt;
        private Category[]? categories;
        public event EventHandler? UCEditCategoriesClicked;

        public UCEditCategories()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCEditCategories_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;

            List<Category> list = new();

            string categoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Request = "update",
                    }
                    );
            pt.SetProtocolID("questions");
            pt.Data = categoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            categories = JsonConvert.DeserializeObject<Category[]>(response);
            if (categories != null)
            {
                for (int i = 0; i < categories.Length; i++)
                {
                    list.Add(new Category()
                    {
                        Codice = categories[i].Codice,
                        Name = categories[i].Name,
                        Price = categories[i].Price
                    });
                }
            }
            else
            {
                Debug.Print("Categories are null");
            }

            dataGridView.DataSource = categories;
            dataGridView.Columns[0].ReadOnly = true;
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int colIndex = dataGridView.CurrentCell.ColumnIndex;

            if (dataGridView.CurrentCell.Value != null)
            {
                string editCategoryJson = System.Text.Json.JsonSerializer.Serialize(
                        new
                        {
                            Codice = dataGridView.CurrentRow.Cells[0].Value,
                            CampoModificato = dataGridView.Columns[colIndex].HeaderText.ToLower(),
                            NuovoValore = dataGridView.CurrentCell.Value.ToString()
                        }
                        );
                pt.SetProtocolID("editCategories");
                pt.Data = editCategoryJson;
                SocketTCP.Send(pt.ToString());
                string response = SocketTCP.Receive();

                MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (response.Equals("Success"))
                {
                    UCEditCategoriesClicked?.Invoke(this, e);
                }
          
                }
                else
            {
                MessageBox.Show("Modifica non valida", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                UCEditCategories_Load(this, e);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            string deleteCategoryJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Codice = dataGridView.CurrentRow.Cells[0].Value,
                    }
                    );
            pt.SetProtocolID("deleteCategories");
            pt.Data = deleteCategoryJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            UCEditCategoriesClicked?.Invoke(this, e);
            UCEditCategories_Load(this, e);
        }
    }
}
