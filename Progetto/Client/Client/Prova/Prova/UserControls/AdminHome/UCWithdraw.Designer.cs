﻿namespace Prova.UserControls.AdminHome
{
    partial class UCWithdraw
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTokenBalance = new System.Windows.Forms.TextBox();
            this.numWithdrawToken = new System.Windows.Forms.NumericUpDown();
            this.btnWithdrawToken = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtETHBalance = new System.Windows.Forms.TextBox();
            this.numWithdrawWei = new System.Windows.Forms.NumericUpDown();
            this.btnWithdrawETH = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numWithdrawToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWithdrawWei)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Token nel Contratto:";
            // 
            // txtTokenBalance
            // 
            this.txtTokenBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtTokenBalance.Location = new System.Drawing.Point(143, 21);
            this.txtTokenBalance.Name = "txtTokenBalance";
            this.txtTokenBalance.ReadOnly = true;
            this.txtTokenBalance.Size = new System.Drawing.Size(127, 23);
            this.txtTokenBalance.TabIndex = 1;
            // 
            // numWithdrawToken
            // 
            this.numWithdrawToken.Location = new System.Drawing.Point(276, 21);
            this.numWithdrawToken.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numWithdrawToken.Name = "numWithdrawToken";
            this.numWithdrawToken.Size = new System.Drawing.Size(120, 23);
            this.numWithdrawToken.TabIndex = 3;
            // 
            // btnWithdrawToken
            // 
            this.btnWithdrawToken.Location = new System.Drawing.Point(409, 21);
            this.btnWithdrawToken.Name = "btnWithdrawToken";
            this.btnWithdrawToken.Size = new System.Drawing.Size(75, 23);
            this.btnWithdrawToken.TabIndex = 4;
            this.btnWithdrawToken.Text = "Preleva";
            this.btnWithdrawToken.UseVisualStyleBackColor = true;
            this.btnWithdrawToken.Click += new System.EventHandler(this.btnWithdraw_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "WEI nel Contratto:";
            // 
            // txtETHBalance
            // 
            this.txtETHBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtETHBalance.Location = new System.Drawing.Point(143, 63);
            this.txtETHBalance.Name = "txtETHBalance";
            this.txtETHBalance.ReadOnly = true;
            this.txtETHBalance.Size = new System.Drawing.Size(127, 23);
            this.txtETHBalance.TabIndex = 6;
            // 
            // numWithdrawWei
            // 
            this.numWithdrawWei.Location = new System.Drawing.Point(276, 63);
            this.numWithdrawWei.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numWithdrawWei.Name = "numWithdrawWei";
            this.numWithdrawWei.Size = new System.Drawing.Size(120, 23);
            this.numWithdrawWei.TabIndex = 7;
            // 
            // btnWithdrawETH
            // 
            this.btnWithdrawETH.Location = new System.Drawing.Point(409, 63);
            this.btnWithdrawETH.Name = "btnWithdrawETH";
            this.btnWithdrawETH.Size = new System.Drawing.Size(75, 23);
            this.btnWithdrawETH.TabIndex = 8;
            this.btnWithdrawETH.Text = "Preleva";
            this.btnWithdrawETH.UseVisualStyleBackColor = true;
            this.btnWithdrawETH.Click += new System.EventHandler(this.btnWithdrawETH_Click);
            // 
            // UCWithdraw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnWithdrawETH);
            this.Controls.Add(this.numWithdrawWei);
            this.Controls.Add(this.txtETHBalance);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnWithdrawToken);
            this.Controls.Add(this.numWithdrawToken);
            this.Controls.Add(this.txtTokenBalance);
            this.Controls.Add(this.label1);
            this.Name = "UCWithdraw";
            this.Size = new System.Drawing.Size(900, 400);
            this.Load += new System.EventHandler(this.UCStats_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numWithdrawToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWithdrawWei)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox txtTokenBalance;
        private NumericUpDown numWithdrawToken;
        private Button btnWithdrawToken;
        private Label label2;
        private TextBox txtETHBalance;
        private NumericUpDown numWithdrawWei;
        private Button btnWithdrawETH;
    }
}
