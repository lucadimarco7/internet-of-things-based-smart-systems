﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using System.Diagnostics;

namespace Prova.UserControls.AdminHome
{
    public partial class UCEditUsers : UserControl
    {
        private readonly Protocol pt;
        private User[]? users;

        public UCEditUsers()
        {
            InitializeComponent();
            pt = new Protocol();
        }

        private void UCEditUsers_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            
            List<User> list = new();
            string userJson = System.Text.Json.JsonSerializer.Serialize(new
            {
                request = "all"
            });
            pt.SetProtocolID("users");
            pt.Data = userJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            users = JsonConvert.DeserializeObject<User[]>(response);

            if (users != null)
            {
                for (int i = 0; i < users.Length; i++)
                {
                    list.Add(new User()
                    {
                        Codice = users[i].Codice,
                        WalletAddress = users[i].WalletAddress,
                        Nome = users[i].Nome,
                        Exp = users[i].Exp,
                        LevelUser = users[i].LevelUser,
                        Balance = users[i].Balance
                    });
                }
            }
            else
            {
                Debug.Print("Users are null");
            }

            dataGridView.DataSource = users;
            dataGridView.Columns[3].Visible = false;
            dataGridView.Columns[4].Visible = false;
            dataGridView.Columns[5].Visible = false;
            dataGridView.Columns[0].ReadOnly = true;
            dataGridView.Columns[1].ReadOnly = true;
            dataGridView.Columns[2].ReadOnly = true;
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int colIndex = dataGridView.CurrentCell.ColumnIndex;

            string editUserJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Codice = dataGridView.CurrentRow.Cells[0].Value,
                        CampoModificato = dataGridView.Columns[colIndex].HeaderText.ToLower(),
                        NuovoValore = dataGridView.CurrentCell.Value.ToString()
                    }
                    );
            pt.SetProtocolID("editProfile");
            pt.Data = editUserJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();
            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!response.Equals("Success"))
                UCEditUsers_Load(this, e);
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            string deleteUserJson = System.Text.Json.JsonSerializer.Serialize(
                    new
                    {
                        Codice = dataGridView.CurrentRow.Cells[0].Value,
                    }
                    );
            pt.SetProtocolID("deleteUsers");
            pt.Data = deleteUserJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            MessageBox.Show(response, "Conferma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            UCEditUsers_Load(this, e);
        }
    }
}
