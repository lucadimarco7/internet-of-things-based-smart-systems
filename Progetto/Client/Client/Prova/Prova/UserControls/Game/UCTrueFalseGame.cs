﻿using Prova.Connection;
using Prova.Data;
using Prova.Utils;
using System.Diagnostics;
using JsonSerializer = System.Text.Json.JsonSerializer;
using Newtonsoft.Json;
using System.Data;

namespace Prova.UserControls.Game
{
    public partial class UCTrueFalseGame : UserControl
    {
        private readonly Protocol pt;
        private readonly List<Question> list = new();
        private readonly string category;
        private List<String> domande = new();
        private List<String> risposte = new();
        private int count = 0;

        public UCTrueFalseGame(List<Question> list, string category)
        {
            InitializeComponent();
            pt = new Protocol();
            this.list = list;
            this.category = category;
        }

        private void UCTrueFalseGame_Load(object sender, EventArgs e)
        {
            domande = new();
            risposte = new();
            count = 0;

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Testo != null && list[i].Risposta != null && list[i].Risposta2 != null &&
                    list[i].Risposta3 != null && list[i].RispostaCorretta != null)
                {
                    domande.Add(list[i].Testo!);
                    String[] arr = { list[i].Risposta!, list[i].RispostaCorretta! };
                    Random random = new();
                    arr = arr.OrderBy(x => random.Next()).ToArray();
                    risposte.Add(arr[0]);
                }
                else
                {
                    Debug.Print("Question is null");
                }
            }

            txtQuestion.Text = domande[count];
            txtAnswer.Text = risposte[count];
        }

        private void End_Click(object? sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void ButtonTrue_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            if (txtAnswer.Text.Equals(list[count].RispostaCorretta))
            {
                btnTrue.BackColor = Color.Green;
                lblPoints.Text = (Int16.Parse(lblPoints.Text) + 1).ToString();
                Wait.wait(1000);

                if (count < list.Count - 2)
                {
                    count++;
                    txtQuestion.Text = domande[count];
                    txtAnswer.Text = risposte[count];
                    btnTrue.BackColor = Color.Transparent;
                }
                else
                {
                    GameEnd();
                }
            }
            else
            {
                btnTrue.BackColor = Color.Red;
                Wait.wait(1000);
                GameEnd();
            }

            this.Enabled = true;
        }

        private void ButtonFalse_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            if (!txtAnswer.Text.Equals(list[count].RispostaCorretta))
            {
                btnFalse.BackColor = Color.Green;
                lblPoints.Text = (Int16.Parse(lblPoints.Text) + 1).ToString();
                Wait.wait(1000);

                if (count < list.Count - 2)
                {
                    count++;
                    txtQuestion.Text = domande[count];
                    txtAnswer.Text = risposte[count];
                    btnFalse.BackColor = Color.Transparent;
                }
                else
                {
                    GameEnd();
                }
            }
            else
            {
                btnFalse.BackColor = Color.Red;
                Wait.wait(1000);
                GameEnd();
            }

            this.Enabled = true;
        }

        private void GameEnd()
        {
            this.Enabled = true;

            UCEndGame ucEndGame = new UCEndGame();
            ucEndGame.UCEndGameButtonClicked += new EventHandler(End_Click);

            pt.SetProtocolID("score");
            string scoreJson = JsonSerializer.Serialize(
                new
                {
                    Game = "tf",
                    Category = category,
                    Punteggio = Int16.Parse(lblPoints.Text),
                }
            );
            pt.Data = scoreJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            Results results = JsonConvert.DeserializeObject<Results>(response)!;

            if (results.ResUpdate && results.ChString != null && results.LevelUser != null &&
                Singleton.Instance.User != null && Singleton.Instance.User.LevelUser != null)
            {
                Singleton.Instance.User.LevelUser!.Level = results.LevelUser.Level;
                Singleton.Instance.User.LevelUser!.Exp = results.LevelUser.Exp;
                Singleton.Instance.User.Balance = results.Balance;
                ucEndGame.Setlabel(results.ChString);
            }
            else
                MessageBox.Show("Sincronizzazione con il server fallita", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

            this.Controls.Clear();
            this.Controls.Add(ucEndGame);
        }
    }
}
