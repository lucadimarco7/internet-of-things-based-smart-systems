﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using JsonSerializer = System.Text.Json.JsonSerializer;
using System.Diagnostics;

namespace Prova.UserControls.Game
{
    public partial class UCHangmanGame : UserControl
    {
        private readonly Protocol pt;
        private readonly List<Question> list = new();
        private readonly string category = String.Empty;
        private List<Label> letter = new();
        private bool wordGuessed;

        public UCHangmanGame(List<Question> list, string category)
        {
            InitializeComponent();
            pt = new Protocol();
            this.list = list;
            this.category = category;
        }

        private void UCHangmanGame_Load(object sender, EventArgs e)
        {
            txtQuestion.Text = list[0].Testo;
            letter = new List<Label>();
            if (list[0].RispostaCorretta != null)
            {
                for (int i = 0; i < list[0].RispostaCorretta!.Length; i++)
                {
                    Label l = new();
                    if (!list[0].RispostaCorretta![i].ToString().Equals(" "))
                        l.Text = "_";
                    else
                        l.Text = " ";
                    l.AutoSize = true;
                    letter.Add(l);
                    flowLayoutPanel1.Controls.Add(l);
                }
            }
            else
            {
                Debug.Print("Correct option is null");
            }
        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
            bool letterGuessed = false;
            wordGuessed = true;
            if (list[0].RispostaCorretta != null)
            {
                for (int i = 0; i < letter.Count; i++)
                {
                    if (list[0].RispostaCorretta![i].ToString().ToUpper().Equals(txtLetter.Text.ToUpper()))
                    {
                        letter[i].Text = txtLetter.Text.ToUpper();
                        letterGuessed = true;
                    }
                }
                for (int i = 0; i < letter.Count; i++)
                {
                    if (!list[0].RispostaCorretta![i].ToString().ToUpper().Equals(letter[i].Text.ToUpper()))
                    {
                        wordGuessed = false;
                    }
                }
                if (!letterGuessed)
                    lblLife.Text = ((Int16.Parse(lblLife.Text)) - 1).ToString();
                if ((Int16.Parse(lblLife.Text)) < 0 || wordGuessed)
                {
                    btnTest.Enabled = false;
                    btnTestAnswer.Enabled = false;
                    if (wordGuessed)
                    {
                        foreach (Label l in letter)
                            l.ForeColor = Color.Green;
                        Utils.Wait.wait(2000);
                    }
                    GameEnd();
                }
                txtLetter.Text = "";
            }
            else
            {
                Debug.Print("Correct option is null");
            }
        }

        private void BtnTestAnswer_Click(object sender, EventArgs e)
        {
            wordGuessed = false;
            if (list[0].RispostaCorretta != null)
            {
                if (list[0].RispostaCorretta!.ToString().ToUpper().Equals(txtAnswer.Text.ToUpper()))
                {
                    for (int i = 0; i < list[0].RispostaCorretta!.Length; i++)
                        letter[i].Text = list[0].RispostaCorretta![i].ToString().ToUpper();
                    wordGuessed = true;
                }
                else
                {
                    lblLife.Text = ((Int16.Parse(lblLife.Text)) - 1).ToString();
                }
                if ((Int16.Parse(lblLife.Text)) < 0 || wordGuessed)
                {
                    btnTest.Enabled = false;
                    btnTestAnswer.Enabled = false;
                    if (wordGuessed)
                    {
                        foreach (Label l in letter)
                            l.ForeColor = Color.Green;
                        Utils.Wait.wait(2000);
                    }
                    GameEnd();
                }
                txtAnswer.Text = "";
            }
            else
            {
                Debug.Print("Correct option is null");
            }
        }

        private void GameEnd()
        {
            UCEndGame ucEndGame = new();
            ucEndGame.UCEndGameButtonClicked += new EventHandler(End_Click);

            pt.SetProtocolID("score");
            string scoreJson = JsonSerializer.Serialize(
                new
                {
                    Game = "im",
                    Category = category,
                    Punteggio = letter.Count,
                    UsedHelps = Int16.Parse(lblLife.Text)
                }
            );
            pt.Data = scoreJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            Results results = JsonConvert.DeserializeObject<Results>(response)!;
            if (results.ResUpdate && results.ChString != null && results.LevelUser != null &&
                Singleton.Instance.User != null && Singleton.Instance.User.LevelUser != null)
            {
                Singleton.Instance.User.LevelUser!.Level = results.LevelUser.Level;
                Singleton.Instance.User.LevelUser!.Exp = results.LevelUser.Exp;
                Singleton.Instance.User.Balance = results.Balance;
                ucEndGame.Setlabel(results.ChString);
            }
            else
                MessageBox.Show("Sincronizzazione con il server fallita", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

            this.Controls.Clear();
            this.Controls.Add(ucEndGame);
        }

        private void End_Click(object? sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }
    }
}
