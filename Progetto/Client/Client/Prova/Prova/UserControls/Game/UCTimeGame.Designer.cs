﻿namespace Prova.UserControls.Game
{
    partial class UCTimeGame
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTimer = new System.Windows.Forms.Label();
            this.helpDoublePoints = new System.Windows.Forms.Button();
            this.helpChangeQuestion = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.helpSolution = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.helpAddTime = new System.Windows.Forms.Button();
            this.lblScore = new System.Windows.Forms.Label();
            this.help5050 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTimer
            // 
            this.lblTimer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTimer.ForeColor = System.Drawing.Color.Black;
            this.lblTimer.Location = new System.Drawing.Point(377, 67);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(30, 27);
            this.lblTimer.TabIndex = 13;
            this.lblTimer.Text = "60";
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // helpDoublePoints
            // 
            this.helpDoublePoints.Location = new System.Drawing.Point(361, 265);
            this.helpDoublePoints.Name = "helpDoublePoints";
            this.helpDoublePoints.Size = new System.Drawing.Size(113, 23);
            this.helpDoublePoints.TabIndex = 18;
            this.helpDoublePoints.Text = "Doppi Punti";
            this.helpDoublePoints.UseVisualStyleBackColor = true;
            this.helpDoublePoints.Click += new System.EventHandler(this.HelpDoublePoints_Click);
            // 
            // helpChangeQuestion
            // 
            this.helpChangeQuestion.Location = new System.Drawing.Point(361, 236);
            this.helpChangeQuestion.Name = "helpChangeQuestion";
            this.helpChangeQuestion.Size = new System.Drawing.Size(113, 23);
            this.helpChangeQuestion.TabIndex = 17;
            this.helpChangeQuestion.Text = "Cambia Domanda";
            this.helpChangeQuestion.UseVisualStyleBackColor = true;
            this.helpChangeQuestion.Click += new System.EventHandler(this.HelpChangeQuestion_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(14, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(330, 240);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // helpSolution
            // 
            this.helpSolution.Location = new System.Drawing.Point(361, 207);
            this.helpSolution.Name = "helpSolution";
            this.helpSolution.Size = new System.Drawing.Size(113, 23);
            this.helpSolution.TabIndex = 16;
            this.helpSolution.Text = "Soluzione";
            this.helpSolution.UseVisualStyleBackColor = true;
            this.helpSolution.Click += new System.EventHandler(this.HelpSolution_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(361, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "Score:";
            // 
            // helpAddTime
            // 
            this.helpAddTime.Location = new System.Drawing.Point(361, 178);
            this.helpAddTime.Name = "helpAddTime";
            this.helpAddTime.Size = new System.Drawing.Size(113, 23);
            this.helpAddTime.TabIndex = 15;
            this.helpAddTime.Text = "+20 sec";
            this.helpAddTime.UseVisualStyleBackColor = true;
            this.helpAddTime.Click += new System.EventHandler(this.HelpAddTime_Click);
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblScore.ForeColor = System.Drawing.Color.Black;
            this.lblScore.Location = new System.Drawing.Point(427, 9);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(18, 19);
            this.lblScore.TabIndex = 12;
            this.lblScore.Text = "0";
            // 
            // help5050
            // 
            this.help5050.Location = new System.Drawing.Point(361, 149);
            this.help5050.Name = "help5050";
            this.help5050.Size = new System.Drawing.Size(113, 23);
            this.help5050.TabIndex = 14;
            this.help5050.Text = "50:50";
            this.help5050.UseVisualStyleBackColor = true;
            this.help5050.Click += new System.EventHandler(this.Help5050_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::QuizApp.Properties.Resources.timer;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(363, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 64);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(361, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 18);
            this.label2.TabIndex = 20;
            this.label2.Text = "Aiuti";
            // 
            // UCTimeGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.helpDoublePoints);
            this.Controls.Add(this.helpChangeQuestion);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.helpSolution);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.helpAddTime);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.help5050);
            this.Controls.Add(this.pictureBox1);
            this.Name = "UCTimeGame";
            this.Size = new System.Drawing.Size(491, 299);
            this.Load += new System.EventHandler(this.UCTimeGame_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblTimer;
        private Button helpDoublePoints;
        private Button helpChangeQuestion;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button helpSolution;
        private Label label1;
        private Button helpAddTime;
        private Label lblScore;
        private Button help5050;
        private PictureBox pictureBox1;
        private Label label2;
    }
}
