﻿using Prova.Connection;
using Prova.Data;
using System.Data;
using Prova.Utils;
using System.Diagnostics;
using JsonSerializer = System.Text.Json.JsonSerializer;
using Newtonsoft.Json;

namespace Prova.UserControls.Game
{
    public partial class UCClassicGame : UserControl
    {
        private readonly Protocol pt;
        private readonly List<UCGameModel> ucGames = new();
        private readonly List<Question> list = new();
        private readonly string category;
        private int helpNum = 0;
        private int min = 2;
        private bool doublePoints = false;

        public UCClassicGame(List<Question> list, string category)
        {
            InitializeComponent();
            pt = new Protocol();
            this.list = list;
            this.category = category;
        }

        private void UCClassicGame_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < list.Count; i++)
            {
                ucGames.Add(new UCGameModel());
                String[] arr = { list[i].Risposta!, list[i].Risposta2!, list[i].Risposta3!, list[i].RispostaCorretta! };
                Random random = new Random();
                arr = arr.OrderBy(x => random.Next()).ToArray();
                ucGames[i].SetFields(list[i].Testo!, arr[0], arr[1], arr[2], arr[3]);
                ucGames[i].SetCorrectOption = list[i].RispostaCorretta!;
                ucGames[i].UCGameButtonClicked += new EventHandler(GameFunc);
            }
            GameFunc(this, new EventArgs());
        }

        private void GameFunc(object? sender, EventArgs e)
        {
            this.Enabled = false;

            if (flowLayoutPanel1.Contains(ucGames[0]))
            {
                if (ucGames[0].IsCorrectAnswer)
                {
                    if (doublePoints)
                    {
                        lblScore.Text = (Int16.Parse(lblScore.Text) + 1).ToString();
                        doublePoints = false;
                    }
                    lblScore.Text = (Int16.Parse(lblScore.Text) + 1).ToString();
                }
                else
                {
                    Wait.wait(1000);
                    GameEnd();
                }
            }

            if (!flowLayoutPanel1.Contains(ucGames[0]))
                flowLayoutPanel1.Controls.Add(ucGames[0]);
            else if (ucGames.Count > min)
            {
                ucGames.RemoveAt(0);
                Wait.wait(1000);
                flowLayoutPanel1.Controls.Clear();
                flowLayoutPanel1.Controls.Add(ucGames[0]);
            }
            else
            {
                GameEnd();
            }

            this.Enabled = true;
        }

        private void GameEnd()
        {
            this.Enabled = true;

            UCEndGame ucEndGame = new();
            ucEndGame.UCEndGameButtonClicked += new EventHandler(End_Click);
            pt.SetProtocolID("score");
            string scoreJson = JsonSerializer.Serialize(
                new
                {
                    Game = "cl",
                    Category = category,
                    Punteggio = Int16.Parse(lblScore.Text),
                    UsedHelps = helpNum
                }
            );
            pt.Data = scoreJson;
            SocketTCP.Send(pt.ToString());
            string response = SocketTCP.Receive();

            Results results = JsonConvert.DeserializeObject<Results>(response)!;

            if (results.ResUpdate && results.ChString != null && results.LevelUser != null &&
                Singleton.Instance.User != null && Singleton.Instance.User.LevelUser != null)
            {
                Singleton.Instance.User.LevelUser!.Level = results.LevelUser.Level;
                Singleton.Instance.User.LevelUser!.Exp = results.LevelUser.Exp;
                Singleton.Instance.User.Balance = results.Balance;
                ucEndGame.Setlabel(results.ChString);
            }
            else
                MessageBox.Show("Sincronizzazione con il server fallita", "Errore",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

            this.Controls.Clear();
            this.Controls.Add(ucEndGame);
        }

        private void End_Click(object? sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void Help5050_Click(object sender, EventArgs e)
        {
            helpNum++;
            help5050.Visible = false;
            int i = 0;
            foreach (Button b in ucGames[0].Controls.OfType<Button>())
            {
                if (!b.Text.Equals(ucGames[0].GetCorrectOption()))
                {
                    b.Visible = false;
                    i++;
                }
                if (i == 2)
                    return;
            }
        }

        private void HelpSolution_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpSolution.Visible = false;
            foreach (Button b in ucGames[0].Controls.OfType<Button>())
            {
                if (b.Text.Equals(ucGames[0].GetCorrectOption()))
                {
                    b.PerformClick();
                }
            }
        }

        private void HelpChangeQuestion_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpChangeQuestion.Visible = false;
            min = 1;
            ucGames.RemoveAt(0);
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel1.Controls.Add(ucGames[0]);
        }

        private void HelpDoublePoints_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpDoublePoints.Visible = false;
            doublePoints = true;
        }
    }
}
