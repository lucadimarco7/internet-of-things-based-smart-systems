﻿using Newtonsoft.Json;
using Prova.Connection;
using Prova.Data;
using Prova.Utils;
using System.Data;
using System.Diagnostics;
using Timer = System.Windows.Forms.Timer;
using JsonSerializer = System.Text.Json.JsonSerializer;
using QuizApp.Properties;

namespace Prova.UserControls.Game
{
    public partial class UCTimeGame : UserControl
    {
        private readonly Protocol pt;
        private readonly List<UCGameModel> ucGames = new();
        private readonly List<Question> list = new();
        private readonly string category;
        private int timer;
        private Timer? myTimer;
        private int longestSeries = 0;
        private int currentSeries = 0;
        private int helpNum = 0;
        private int min = 2;
        private bool doublePoints = false;
        private bool online = false;

        public UCTimeGame(List<Question> list, string category, bool online = false)
        {
            InitializeComponent();
            pt = new Protocol();
            this.list = list;
            this.category = category;
            this.online = online;
        }
        private void UCTimeGame_Load(object sender, EventArgs e)
        {
            timer = 10;

            for (int i = 0; i < list.Count; i++)
            {
                ucGames.Add(new UCGameModel());
                String[] arr = { list[i].Risposta!, list[i].Risposta2!, list[i].Risposta3!, list[i].RispostaCorretta! };
                Random random = new Random();
                arr = arr.OrderBy(x => random.Next()).ToArray();
                ucGames[i].SetFields(list[i].Testo!, arr[0], arr[1], arr[2], arr[3]);
                ucGames[i].SetCorrectOption = list[i].RispostaCorretta!;
                ucGames[i].UCGameButtonClicked += new EventHandler(GameFunc);
            }

            GameFunc(this, new EventArgs());
            this.Visible = true;
            myTimer = new Timer();
            myTimer.Tick += new EventHandler(TimerEventProcessor!);
            myTimer.Interval = 1000;
            myTimer.Start();
        }

        private void GameFunc(object? sender, EventArgs e)
        {
            this.Enabled = false;
            if (flowLayoutPanel1.Contains(ucGames[0]))
            {
                if (ucGames[0].IsCorrectAnswer)
                {
                    if (doublePoints)
                    {
                        lblScore.Text = (Int16.Parse(lblScore.Text) + 1).ToString();
                        doublePoints = false;
                    }
                    lblScore.Text = (Int16.Parse(lblScore.Text) + 1).ToString();
                    currentSeries++;
                    if (currentSeries > longestSeries)
                        longestSeries = currentSeries;
                }
                else
                {
                    currentSeries = 0;
                }
            }

            if (!flowLayoutPanel1.Contains(ucGames[0]))
                flowLayoutPanel1.Controls.Add(ucGames[0]);
            else if (ucGames.Count > min && timer > 0)
            {
                ucGames.RemoveAt(0);
                Wait.wait(1000);
                flowLayoutPanel1.Controls.Clear();
                flowLayoutPanel1.Controls.Add(ucGames[0]);
            }
            else
            {
                GameEnd();
            }

            this.Enabled = true;
        }

        async public void GameEnd()
        {

            this.Enabled = false;

            UCEndGame ucEndGame = new();
            ucEndGame.UCEndGameButtonClicked += new EventHandler(End_Click);

            if (myTimer != null)
                myTimer.Stop();
            string scoreJson;
            if (online)
            {
                scoreJson = JsonSerializer.Serialize(
                    new
                    {
                        Punteggio = Int16.Parse(lblScore.Text),
                        LongestSeries = longestSeries,
                        UsedHelps = helpNum
                    }
                );
                pt.SetProtocolID("scoreOnline");
                pt.Data = scoreJson;
                SocketTCP.Send(pt.ToString());
            }
            else
            {
                scoreJson = JsonSerializer.Serialize(
                    new
                    {
                        Game = "tm",
                        Category = category,
                        Punteggio = Int16.Parse(lblScore.Text),
                        LongestSeries = longestSeries,
                        UsedHelps = helpNum
                    }
                );
                pt.SetProtocolID("score");
                pt.Data = scoreJson;
                SocketTCP.Send(pt.ToString());
            }
            string response = "wait";
            if (online)
            {
                this.Controls.Clear();
                UCOnlineWait ucOnlineWait = new UCOnlineWait();
                ucOnlineWait.SetLabel1("In attesa del risultato dell'avversario...\n"+
                    "Non chiudere la finestra o perderai la partita.");
                this.Controls.Add(ucOnlineWait);
                this.Enabled = true;
                while (response.Equals("wait"))
                {
                    await Task.Delay(500);
                    response = SocketTCP.Receive();
                }
                if (response.Equals("EXIT"))
                {
                    return;
                }
                if (response.Equals("err"))
                {
                    return;
                }
            }
            else
            {
                response = SocketTCP.Receive();

            }
            Results results = JsonConvert.DeserializeObject<Results>(response)!;
                if (results.ResUpdate && results.ChString != null && results.LevelUser != null &&
                    Singleton.Instance.User != null && Singleton.Instance.User.LevelUser != null)
                {
                    Singleton.Instance.User.LevelUser!.Level = results.LevelUser.Level;
                    Singleton.Instance.User.LevelUser!.Exp = results.LevelUser.Exp;
                    Singleton.Instance.User.Balance = results.Balance;
                    ucEndGame.Setlabel(results.ChString);
                }
                else
                    MessageBox.Show("Sincronizzazione con il server fallita", "Errore",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);         

            this.Controls.Clear();
            this.Controls.Add(ucEndGame);
        }

        private void End_Click(object? sender, EventArgs e)
        {
            ((Form)this.TopLevelControl).Close();
        }

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer == 0)
            {
                GameEnd();
            }
            else
            {
                timer--;
                lblTimer.Text = timer.ToString();
            }
        }

        public void StopTimer()
        {
            if (myTimer != null)
            {
                myTimer.Stop();
                timer = 0;
                this.Controls.Clear();
            }
        }

        private void Help5050_Click(object sender, EventArgs e)
        {
            helpNum++;
            help5050.Visible = false;
            int i = 0;
            foreach (Button b in ucGames[0].Controls.OfType<Button>())
            {
                if (!b.Text.Equals(ucGames[0].GetCorrectOption()))
                {
                    b.Visible = false;
                    i++;
                }
                if (i == 2)
                    return;
            }
        }

        private void HelpAddTime_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpAddTime.Visible = false;
            timer = timer + 20;
        }

        private void HelpSolution_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpSolution.Visible = false;
            foreach (Button b in ucGames[0].Controls.OfType<Button>())
            {
                if (b.Text.Equals(ucGames[0].GetCorrectOption()))
                {
                    b.PerformClick();
                }
            }
        }

        private void HelpChangeQuestion_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpChangeQuestion.Visible = false;
            min = 1;
            ucGames.RemoveAt(0);
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel1.Controls.Add(ucGames[0]);
        }

        private void HelpDoublePoints_Click(object sender, EventArgs e)
        {
            helpNum++;
            helpDoublePoints.Visible = false;
            doublePoints = true;
        }
    }
}
