﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prova.UserControls
{
    public partial class UCShopCategoryModel : UserControl
    {
        public event EventHandler? UCShopCategoryButtonClicked;

        public UCShopCategoryModel()
        {
            InitializeComponent();
        }

        public void SetName(string name, int price)
        {
            button.Text = name + "\n" + price;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            UCShopCategoryButtonClicked?.Invoke(button.Text, e);
        }
    }
}
