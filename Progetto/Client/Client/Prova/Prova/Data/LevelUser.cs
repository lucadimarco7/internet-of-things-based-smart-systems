﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova.Data
{
    public class LevelUser
    {
        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("exp")]
        public int Exp { get; set; }
    }
}
