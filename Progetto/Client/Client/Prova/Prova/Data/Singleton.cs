﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova.Data
{
    public sealed class Singleton
    {
        private User? user;

        private static Singleton? _instance;

        private Singleton() { }

        public static Singleton Instance
        {
            get
            {
                _instance ??= new Singleton();
                return _instance;
            }
        }

        public User User { get => user!; set => user = value; }
    }
}
