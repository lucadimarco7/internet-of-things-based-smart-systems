﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json;

namespace Prova.Data
{
    public class Question
    {
        [JsonProperty("codice")]
        public string? Codice { get; init; }

        [JsonProperty("categoria")]
        public string? Categoria { get; init; }
        
        [JsonProperty("testo")]
        public string? Testo { get; init; }

        [JsonProperty("risposta")]
        public string? Risposta { get; init; }

        [JsonProperty("risposta2")]
        public string? Risposta2 { get; init; }

        [JsonProperty("risposta3")]
        public string? Risposta3 { get; init; }

        [JsonProperty("rispostaCorretta")]
        public string? RispostaCorretta { get; init; }
    }  
}
