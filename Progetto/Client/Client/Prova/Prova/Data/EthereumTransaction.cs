﻿using System.Threading.Tasks;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.Util;
using Nethereum.RPC.Eth.DTOs;

namespace QuizApp.Data
{
    public class EthereumTransaction
    {
        private readonly Web3 _web3;

        public EthereumTransaction()
        {
            var account = new Account("<your_private_key>");
            _web3 = new Web3(account, "https://goerli.infura.io/v3/3e3ff15835e248edaab6868cfffaf161");
        }

        public async Task<string> SendTransactionAsync(string toAddress, decimal amount)
        {
            var weiAmount = UnitConversion.Convert.ToWei(amount);
            var gasPrice = await _web3.Eth.GasPrice.SendRequestAsync();
            var gasLimit = new HexBigInteger(21000);

            var transactionInput = new TransactionInput
            {
                To = toAddress,
                Value = new HexBigInteger(weiAmount),
                GasPrice = gasPrice,
                Gas = gasLimit
            };

            var transactionHash = await _web3.Eth.TransactionManager.SendTransactionAsync(transactionInput);

            return transactionHash;
        }
    }
}