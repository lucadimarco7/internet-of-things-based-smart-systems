﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova.Data
{
    public class User
    {
        [JsonProperty("codice")]
        public string? Codice { get; set; }

        [JsonProperty("wallet_address")]
        public string? WalletAddress { get; set; }

        [JsonProperty("nome")]
        public string? Nome { get; set; }

        [JsonProperty("password")]
        public string? Password { get; init; }

        [JsonProperty("exp")]
        public int? Exp { get; set; }

        [JsonProperty("leveluser")]
        public LevelUser? LevelUser { get; set; }

        [JsonProperty("balance")]
        public int? Balance { get; set; }

    }
}
