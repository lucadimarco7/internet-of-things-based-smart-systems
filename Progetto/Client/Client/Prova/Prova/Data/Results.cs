﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prova.Data
{
    class Results
    {
        [JsonProperty("resUpdate")]
        public bool ResUpdate { get; init; }

        [JsonProperty("levelUp")]
        public bool LevelUp { get; init; }

        [JsonProperty("leveluser")]
        public LevelUser? LevelUser { get; init; }

        [JsonProperty("balance")]
        public int Balance { get; init; }

        [JsonProperty("chString")]
        public string? ChString { get; init; }
    }
}
